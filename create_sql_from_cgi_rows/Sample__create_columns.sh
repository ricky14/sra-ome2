
scriptBase='./getAllFieldValues_with_container.sh'
folder='txt_for_sql'
out_dir='txt_for_sql'
all_data_file_f=$1

#---------------------------------------------------------------------------

#SAMPLE
#(SampleAccession, Title, Description, Alias, BioSampleID, TaxonID)
#PK: SampleAccession

#./getAllFieldValuesWithRegionsAndIdentifier.sh 'txt_for_sql/Sample_PK.txt' 'txt_for_sql/0_regions.txt' 'experiment_formatted.xml'

sample_pk_file_non_uniq=$folder'/Sample_0_PK.txt'
sample_pk_file_uniq=$folder'/Sample_0_PK_uniq.txt'

echo PK
#create pk file
#useful for other scripts, not useful for db insert
$scriptBase \
$all_data_file_f \
'<SAMPLE ' \
'accession="' \
'"' \
'>' \
>$sample_pk_file_non_uniq

#take uniq pk file
#Sample -> SampleAccession
cat $sample_pk_file_non_uniq \
| uniq \
| sort \
| uniq \
>$sample_pk_file_uniq


echo Title
#Sample -> Title
#NB some titles are lost:
#we DO catch the one stored as: 
#   <.*sample_title="somesampletitle".*>
#we DO NOT catch the one stored as (cuz of multiline):
#   <TAG>sample_title</TAG>
#   <VALUE>somesampletitle</VALUE>

$scriptBase \
$all_data_file_f \
'<SAMPLE_NAME>' \
'<SCIENTIFIC_NAME>' \
'</SCIENTIFIC_NAME>' \
'</SAMPLE_NAME>' \
>$out_dir'/Sample_1_Title.txt'

echo Description ## c'è ma su pochi
#Sample -> Description
$scriptBase \
$all_data_file_f \
'<EXPERIMENT_PACKAGE>' \
'<DESCRIPTION>' \
'</DESCRIPTION>' \
'</EXPERIMENT_PACKAGE>' \
>>$out_dir'/Sample_2_Description.txt'

echo Alias
#Sample -> Alias
$scriptBase \
$all_data_file_f \
'<SAMPLE ' \
'alias="' \
'"' \
'>' \
>$out_dir'/Sample_3_Alias.txt'

echo BioSampleID
#Sample -> BioSampleID
#cat $all_data_file_f \
#| egrep '<SAMPLE_DESCRIPTOR' -A 5 \
#| egrep '<EXTERNAL_ID .*namespace="BioSample"[^>]*>' \
#| sed 's/^.*<EXTERNAL_ID .*">//' \
#| sed 's:</EXTERNAL_ID>.*::' \
#> $out_dir'/Sample_4_BioSampleID.txt'

$scriptBase \
$all_data_file_f \
'<SAMPLE ' \
'<EXTERNAL_ID namespace="BioSample">' \
'<' \
'</SAMPLE>' \
>$out_dir'/Sample_4_BioSampleID.txt'

echo TaxonID
#Sample -> TaxonID
$scriptBase \
$all_data_file_f \
'<SAMPLE ' \
'<TAXON_ID>' \
'</TAXON_ID>' \
'</SAMPLE>' \
>$out_dir'/Sample_5_TaxonID.txt'

./create_insert_into_sql_from_columns.sh Sample.sql Sample \
SampleAccession,Title,Description,Alias,BioSampleID,TaxonID \
$out_dir/Sample_0_PK.txt,$out_dir/Sample_1_Title.txt,$out_dir/Sample_2_Description.txt,$out_dir/Sample_3_Alias.txt,$out_dir/Sample_4_BioSampleID.txt,$out_dir/Sample_5_TaxonID.txt