
all_data_file_f=$1
out_dir="./txt_for_sql/"

#---------------------------------------------------------------------------

#STUDYSUPERVISOR
#(StudyAccession, InstitutionName)
#PK: (StudyAccession, InstitutionName)

study_pk_file=$out_dir'Study_0_PK.txt'
institution_pk_file=$out_dir'Institution_0_PK.txt'
studyinstitution_pk_file_non_uniq=$out_dir'StudySupervisor_0_PK.txt'

study_pk1_file=$out_dir'StudySupervisor_0_PK1_Study.txt'
istitution_pk2_file=$out_dir'StudySupervisor_0_PK2_Institution.txt'

studysupervisor_pk_file_uniq=$out_dir'StudySupervisor_0_PK_uniq.txt'
studysupervisor_pk_file_uniq_notnull=$out_dir'StudySupervisor_0_PK_uniq_notnull.txt'


echo paste
#create association
paste -d',' $study_pk_file  $institution_pk_file \
>$studyinstitution_pk_file_non_uniq

echo unique
#take uniq rows
#cat $studysample_pk_file \
#| sort -u  \
#>$studysample_pk_file_uniq

length=$(wc -l $studyinstitution_pk_file_non_uniq | awk '{print $1;}')
#echo $length
[ -e temp.txt ] && rm temp.txt
for ((i=0; i*1000<length; i++))
do
    sed -n "$((i*1000+1)),$(((i+1)*1000))p;$(((i+1)*1000+1))q" $studyinstitution_pk_file_non_uniq \
    | sort \
    >> temp.txt
done

cat temp.txt \
| sort -u \
>$studysupervisor_pk_file_uniq

rm temp.txt

cat $studysupervisor_pk_file_uniq \
| sed s/.*','$//  \
| grep -v ^$ \
>$studysupervisor_pk_file_uniq_notnull

cat $studysupervisor_pk_file_uniq_notnull \
| cut -d',' -f1 \
>$study_pk1_file

cat $studysupervisor_pk_file_uniq_notnull \
| cut -d',' -f2- \
>$istitution_pk2_file


./create_insert_into_sql_from_columns.sh StudySupervisor.sql StudySupervisor \
StudyAccession,InstitutionName \
$study_pk1_file,$istitution_pk2_file