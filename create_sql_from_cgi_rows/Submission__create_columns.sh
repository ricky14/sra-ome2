
scriptBase='./getAllFieldValues_with_container.sh'
out_dir="txt_for_sql"
all_data_file_f=$1

#---------------------------------------------------------------------------

#SUBMISSION
#(SubmissionAccession)
#PK: SampleAccession

submission_pk_file_non_uniq='/Submission_0_PK.txt'
submission_pk_file_uniq='/Submission_0_PK_uniq.txt'

echo PK
#create pk file
#Submission -> SubmissionAccession
$scriptBase \
$all_data_file_f \
'<SUBMISSION ' \
'accession="' \
'"' \
'>' \
>$out_dir$submission_pk_file_non_uniq

cat $out_dir$submission_pk_file_non_uniq \
| uniq | sort | uniq \
>$out_dir$submission_pk_file_uniq

./create_insert_into_sql_from_columns.sh Submission.sql Submission \
SubmissionAccession $out_dir$submission_pk_file_uniq