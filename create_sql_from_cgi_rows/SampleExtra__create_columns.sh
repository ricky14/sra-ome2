
scriptBase='./getAllFieldValues_with_container.sh'
folder='txt_for_sql'
out_dir='txt_for_sql'
all_data_file_f=$1
temp_file="sampextra.txt"

echo -n "" > $temp_file
#---------------------------------------------------------------------------

#SAMPLEEXTRA
#(SampleAccession, SampleDate, LocalityName, LatLon)
#PK: SampleAccession

sample_pk_file_non_uniq=$folder'/SampleExtra_0_PK.txt'

echo PK
#create pk file
#useful for other scripts, not useful for db insert
$scriptBase \
$all_data_file_f \
'<SAMPLE ' \
'accession="' \
'"' \
'>' \
>$sample_pk_file_non_uniq

while read samp_acc;
do 
    grep "$samp_acc" "$all_data_file_f" \
    | sed "s/.*<SAMPLE_ATTRIBUTES>//" \
    | sed "s/<\/SAMPLE_ATTRIBUTES>.*//" \
    | sed "s/<\/SAMPLE_ATTRIBUTE><SAMPLE_ATTRIBUTE>/<\/SAMPLE_ATTRIBUTE>\\n<SAMPLE_ATTRIBUTE>/g" \
    | sed "s/<SAMPLE_ATTRIBUTE>/<SAMPLE accession=\"$samp_acc\"><SAMPLE_ATTRIBUTE>/" \
    >> $temp_file

done < $sample_pk_file_non_uniq

echo PK
#create pk file
#useful for other scripts, not useful for db insert
$scriptBase \
$temp_file \
'<SAMPLE ' \
'accession="' \
'"' \
'>' \
>$out_dir'/SampleExtra_0_PK.txt'

echo Tag

$scriptBase \
$temp_file \
'<SAMPLE_ATTRIBUTE>' \
'<TAG>' \
'<\/TAG>' \
'<\/SAMPLE_ATTRIBUTE>' \
>$out_dir'/SampleExtra_1_SampleTags.txt'


echo Value 

$scriptBase \
$temp_file \
'<SAMPLE_ATTRIBUTE>' \
'<VALUE>' \
'<\/VALUE>' \
'<\/SAMPLE_ATTRIBUTE>' \
>$out_dir'/SampleExtra_2_SampleValues.txt'

rm $temp_file

./create_insert_into_sql_from_columns.sh SampleExtra.sql SampleExtra \
SampleAccession,Tag,Value \
$out_dir/SampleExtra_0_PK.txt,$out_dir/SampleExtra_1_SampleTags.txt,$out_dir/SampleExtra_2_SampleValues.txt