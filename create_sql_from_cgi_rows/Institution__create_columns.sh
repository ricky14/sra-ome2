
scriptBase='./getAllFieldValues_with_container.sh'
all_data_file_f=$1
out_dir='txt_for_sql'
#---------------------------------------------------------------------------

#INSTITUION
#(Institution)
#PK: InstitutionName

institution_pk_file_uniq=$out_dir'/Institution_0_PK_uniq.txt'
institution_pk_file=$out_dir'/Institution_0_PK.txt'

#create pk file
#useful for other scripts, not useful for db insert
#cat $all_data_file_f \
#| egrep '<SUBMISSION .*center_name="' \
#| sed s/.*'center_name="'//g \
#| sed s/'"'.*//g \
#>$institution_pk_file

$scriptBase \
$all_data_file_f \
'<SUBMISSION ' \
'center_name="' \
'"' \
'>' \
> $institution_pk_file

cat $institution_pk_file \
| uniq | sort | uniq \
>$institution_pk_file_uniq

./create_insert_into_sql_from_columns.sh Institution.sql Institution \
InstitutionName $institution_pk_file_uniq
