
scriptBase='./getAllFieldValues_with_container.sh'
out_dir='txt_for_sql'
all_data_file_f=$1


#---------------------------------------------------------------------------

#EXPERIMENT
#ExperimentAccession, Title, Alias, Design, LibraryName, SampleAccession, 
#LibrarySource, LibraryLayout, LibrarySelection, LibraryStrategy, PlatformName, InstrumentName)
#PK: ExperimentAccession

echo PK
#create pk file
#useful for other scripts, not useful for db insert
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'accession="' \
'"' \
'>' \
> $out_dir/'Experiment_0_PK.txt'

echo Title
#Experiment -> Title
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<TITLE>' \
'</TITLE>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_1_Title.txt'

echo Alias
#Experiment -> Alias
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'alias="' \
'"' \
'>' \
> $out_dir/'Experiment_2_Alias.txt'

echo Design
#Experiment -> Design
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<DESIGN_DESCRIPTION>' \
'</DESIGN_DESCRIPTION>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_3_Design.txt'

echo LibraryName
#Experiment -> LibraryName
#$scriptBase \
#$all_data_file \
#'<Library_descriptor>(<LIBRARY_NAME>)?' \
#'</Library_descriptor>' \
#> $out_dir/'Experiment_4_LibraryName.txt'

$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<LIBRARY_NAME>' \
'</LIBRARY_NAME>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_4_LibraryName.txt'


echo SampleAccession
#Experiment -> SampleAccession
$scriptBase \
$all_data_file_f \
'<SAMPLE ' \
'accession="' \
'"' \
'>' \
> $out_dir/'Experiment_5_SampleAccession.txt'

echo LibrarySource
#Experiment -> LibrarySource
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<LIBRARY_SOURCE>' \
'</LIBRARY_SOURCE>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_6_LibrarySource.txt'

echo LibraryLayout
#Experiment -> LibraryLayout
#format is multiline:
#<LIBRARY_LAYOUT>
#   <(SINGLE|PAIRED).*/>
#</LIBRARY_LAYOUT>
#wrote apposite pipe:
#NB after teh research we have to sort them with Exp_PK !!

#cat $all_data_file_f | \
#grep '<LIBRARY_LAYOUT>' \
#| egrep '<(SINGLE|PAIRED).*/>' \
#| sed s/.*'<'// \
#| sed s/[^A-Z].*// \
#> $out_dir/'Experiment_7_LibraryLayout.txt'

$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<LIBRARY_LAYOUT><' \
'/></LIBRARY_LAYOUT>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_7_LibraryLayout.txt'


echo LibrarySelection
#Experiment -> LibrarySelection
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<LIBRARY_SELECTION>' \
'</LIBRARY_SELECTION>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_8_LibrarySelection.txt'

echo LibraryStrategy
#Experiment -> LibraryStrategy
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<LIBRARY_STRATEGY>' \
'</LIBRARY_STRATEGY>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_9_LibraryStrategy.txt'


echo PlatformName
#Experiment -> PlatformName
#cat $all_data_file_f | \
#grep '<PLATFORM>' -A 1 \
#| sed s/\<PLATFORM\>// \
#| sed s/.*'<'// \
#| sed s/[^A-Z].*// \
#| egrep "[a-zA-Z]+" \
#> $out_dir/'Experiment_10_PlatformName.txt'

$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<PLATFORM><' \
'>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_10_PlatformName.txt'


echo InstrumentName
#Experiment -> InstrumentName
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<INSTRUMENT_MODEL>' \
'</INSTRUMENT_MODEL>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_11_InstrumentName.txt'

./create_insert_into_sql_from_columns.sh Experiment.sql Experiment \
ExperimentAccession,Title,Alias,Design,LibraryName,SampleAccession,LibrarySource,LibraryLayout,LibrarySelection,LibraryStrategy,PlatformName,InstrumentName \
$out_dir/Experiment_0_PK.txt,$out_dir/Experiment_1_Title.txt,$out_dir/Experiment_2_Alias.txt,$out_dir/Experiment_3_Design.txt,$out_dir/Experiment_4_LibraryName.txt,$out_dir/Experiment_5_SampleAccession.txt,$out_dir/Experiment_6_LibrarySource.txt,$out_dir/Experiment_7_LibraryLayout.txt,$out_dir/Experiment_8_LibrarySelection.txt,$out_dir/Experiment_9_LibraryStrategy.txt,$out_dir/Experiment_10_PlatformName.txt,$out_dir/Experiment_11_InstrumentName.txt