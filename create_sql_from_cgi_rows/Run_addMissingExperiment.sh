#set -x

all_data_file=$1
file_to_cut=$2
output_file='out.txt'

echo -n >$output_file

i=0

while read line
do
    key=`echo $line | cut -d',' -f1`
    val=`echo $line | cut -d',' -f2-`
    
    echo $i':' $line

    if test -z "$val"
    then
        echo '--- found empty'
        val=`../getOneFieldValueFromIdentifier.sh $all_data_file '<RUN .*accession=\"'$key'\"' '<EXPERIMENT .*accession="' '"' 100`

        #sleep 20
    fi
    echo '--- new value:'
    echo $i':' $key','$val
    echo $key','$val >>$output_file

    i=$(($i + 1))


done <$file_to_cut