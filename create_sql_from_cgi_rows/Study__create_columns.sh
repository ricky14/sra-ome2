
scriptBase='./getAllFieldValues_with_container.sh'
folder='txt_for_sql'
out_dir='txt_for_sql'
all_data_file_f=$1

#---------------------------------------------------------------------------

#STUDY
#(StudyAccession, Title, Abstract, BioProjectID)
#PK: StudyAccession

study_pk_file_non_uniq=$folder'/Study_0_PK.txt'
study_pk_file_uniq=$folder'/Study_0_PK_uniq.txt'

echo PK
#Study -> StudyAccession
#useful for other scripts, not useful for db insert
$scriptBase \
$all_data_file_f \
'<STUDY ' \
'accession="' \
'"' \
'>' \
>$study_pk_file_non_uniq

#take uniq pk file
#Study -> StudyAccession
cat $study_pk_file_non_uniq \
| uniq \
| sort \
| uniq \
>$study_pk_file_uniq

echo Title
#Study -> Title
$scriptBase \
$all_data_file_f \
'<STUDY ' \
'<STUDY_TITLE>' \
'</STUDY_TITLE>' \
'</STUDY>' \
>$out_dir'/Study_1_Title.txt'

echo Abstract
#Study-> Abstract
$scriptBase \
$all_data_file_f \
'<STUDY ' \
'<STUDY_ABSTRACT>' \
'</STUDY_ABSTRACT>' \
'</STUDY>' \
>$out_dir'/Study_2_Abstract.txt'

echo BioProjectID
#Study -> BioProjectID
#cat $all_data_file_f \
#| egrep '<STUDY_REF' -A 5 \
#| egrep '<EXTERNAL_ID .*namespace="BioProject"[^>]*>' \
#| sed 's/^.*<EXTERNAL_ID .*">//' \
#| sed 's:</EXTERNAL_ID>.*::' \
#> $out_dir'/Study_3_BioProjectID.txt'

$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<EXTERNAL_ID namespace="BioProject">' \
'</EXTERNAL_ID>' \
'</EXPERIMENT>' \
>$out_dir'/Study_3_BioProjectID.txt'


./create_insert_into_sql_from_columns.sh Study.sql Study \
StudyAccession,Title,Abstract,BioProjectID \
$out_dir/Study_0_PK.txt,$out_dir/Study_1_Title.txt,$out_dir/Study_2_Abstract.txt,$out_dir/Study_3_BioProjectID.txt