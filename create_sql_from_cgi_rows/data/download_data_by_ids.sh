
dataFileFromSearch=$2'_info.xml'
dataFileFromSearch_f=$2'_info_formatted.xml'
dataFileFromFetch=$2'_fetch.xml'
dataFileFromFetch_f=$2'_fetch_formatted.xml'

ids=$1

echo "Get selected runs"
wget -O $dataFileFromFetch \
'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=sra&id='$ids


status=$?
if [ $status -gt 0 ]
then
    echo 'Error on fetch data download'
    exit $status
fi

echo "Finished download"

