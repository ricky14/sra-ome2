#!/bin/bash
dataFileFromFetch="$1"'_fetch.xml'
dataFileFromFetch_f="$1"'_fetch_formatted.xml'
formatted=$2 ## ('or') for only rows, default with formatted file too 

START_TIME=$SECONDS

dir="./temp/"
rm ./temp/*

temp_file="$1"'_fetch_rows.xml'

[ -e $temp_file ] && rm $temp_file

echo "Sed"
tail -n+3 $dataFileFromFetch \
| sed 's/<\/EXPERIMENT_PACKAGE>/<\/EXPERIMENT_PACKAGE>\n/g' \
| sed 's/<!--.*//g' \
| sed 's/.*-->//g' \
| tr '\n' ' ' \
| sed 's/<\/EXPERIMENT_PACKAGE>/<\/EXPERIMENT_PACKAGE>\n/g' \
> $temp_file


if [[ $# == 2 ]]
then
    if [[ $formatted == 'or' ]]
    then
        echo 'Fixing rows'
        cat $temp_file \
        | sed 's/ </</g' \
        | sed '$d' \
        > temp.xml
        cat temp.xml > $temp_file
        rm temp.xml
        echo 'Created only rows file'
        exit 0
    fi
fi


i=0
file_names='experiment.xml'

echo "Split..."
while read line
do
        if [[ $line =~ '<EXPERIMENT_PACKAGE>' ]]
        then
            i=$((i+1))
            printf -v j "%07d" $i
            echo -ne $i'\r'
        fi
        if [[ $line =~ '</EXPERIMENT_PACKAGE_SET>' ]]
        then
            echo $i
            break
        else
            printf '%s\n' "$line" >> $dir$j'_'$file_names
        fi
done < $temp_file

echo "Split done"
wc -l $temp_file

echo 'Fixing rows'
cat $temp_file \
| sed 's/ </</g' \
| sed '$d' \
> temp.xml
cat temp.xml > $temp_file
rm temp.xml

echo 'Formatting file...'

head -2 $dataFileFromFetch > $dataFileFromFetch_f
echo '<Body_fetch>' > temp2.xml
c=0
for file in $dir*; do

    c=$((c+1))
    echo -ne $c'\r'

    cat $file \
    | xmllint --format - \
    | tail -n+2 \
    >> temp2.xml

    if [ $(expr $c % 200 ) -eq 0 ]; then
        echo '</Body_fetch>' >> temp2.xml
        cat temp2.xml \
        | sed 's/<!.*></</g' \
        | perl -MHTML::Entities -pe 'decode_entities($_);'\
        | tail -n+2 \
        | sed '$d' \
        >> $dataFileFromFetch_f
        echo '<Body_fetch>' > temp2.xml
    fi

done

echo '</Body_fetch>' >> temp2.xml
cat temp2.xml \
| sed 's/<!.*></</g' \
| perl -MHTML::Entities -pe 'decode_entities($_);'\
| tail -n+2 \
| sed '$d' \
>> $dataFileFromFetch_f


echo '</EXPERIMENT_PACKAGE_SET>' >> $dataFileFromFetch_f

echo ''

rm temp2.xml
rm ./temp/*.xml

ELAPSED_TIME=$(($SECONDS - $START_TIME))

echo "Data formatted in" $ELAPSED_TIME"s"

