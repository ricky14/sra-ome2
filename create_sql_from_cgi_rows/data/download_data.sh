
dataFileFromSearch=$2'_info.xml'
dataFileFromSearch_f=$2'_info_formatted.xml'
dataFileFromFetch=$2'_fetch.xml'
dataFileFromFetch_f=$2'_fetch_formatted.xml'
scriptBase="../getAllFieldValues.sh"


query=$(echo "$1" | sed "s/ /+/g")
echo "$query"

wget -O $dataFileFromSearch \
'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=sra&usehistory=y&term='$query

status=$?
if [ $status -gt 0 ]
then
    echo 'Error on first data search'
    exit $status
fi

xmllint --format $dataFileFromSearch > $dataFileFromSearch_f

#Get WebEnv and query_key variable
WebEnv=$($scriptBase "$dataFileFromSearch" '<WebEnv>' '</WebEnv>')
query_key=$($scriptBase "$dataFileFromSearch" '<QueryKey>' '</QueryKey>')

echo "WebEnv" $WebEnv
echo "query_key" $query_key

############### Variabili per decidere quali run scaricare
getAll=1
ids=$($scriptBase "$dataFileFromSearch" '<Id>' '</Id>')
ids="${ids//[$'\t\r\n ']/,}"

#echo "ids: '"$ids"'"
if [ $getAll -eq 1 ];
then
    echo "Get all runs"
    wget -O $dataFileFromFetch \
    "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=sra&query_key=$query_key&WebEnv=$WebEnv"
else
    echo "Get selected runs"
    wget -O $dataFileFromFetch \
    'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=sra&id='$ids
fi

status=$?
if [ $status -gt 0 ]
then
    echo 'Error on fetch data download'
    exit $status
fi

echo "Finished download"

