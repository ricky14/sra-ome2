#set -x

nOfParamsNeeded=4

if test $# -lt $nOfParamsNeeded
then
    echo "usage: $0 <output_file> <table_name> <list: field_names> <list: field_values_files>"
    echo "example: $0 Study_insert.sql Study StudyAccession,BioProjectID,Title,Abstract Study_0_PK_uniq.txt,Study_1_BioProjectID.txt,Study_2_Title.txt,Study_3_Abstract.txt"
    exit 1
fi

output_file=$1
table_name=$2
field_names=$3
IFS=',' read -r -a value_files <<< "$4"

temp_out_file='temp_output.txt'
temp_out_file_helper='temp_output_helper.txt'

nOfFields=${#value_files[@]}

echo -n >$output_file
echo -n >$temp_out_file_helper

for ((i=0; i<$nOfFields; i++))
do
    #escape column:
        #from:  xyz'testsomething
        #to:    E'xyz\'testsomething'
    #add null to empty value:
        #from:  E''
        #to:    null

    #NB use E'' instead of '' allows use of escaper inside value
        #ok     ==> E'some\'thing'
        #error  ==> 'some\'thing'

    #NB: 1st sed: escapes:          from some'thing     to some\'thing
    #NB: 2nd sed: adds E' start:    from some\'thing    to E'some\'thing
    #NB: 3rd sed: adds '  end:      from E'some\'thing  to E'some\'thing'
    #NB: 4th sed: adds null:        from E''            to null
    temp_value_file='temp_value_file.txt'
    sed s:\':'\\'\':g ${value_files[i]} \
    | sed s:^:E\': \
    | sed s:$:\': \
    | sed s:E\'\':null:g \
    >$temp_value_file
    
    if test $i -eq 0
    then
        cat $temp_value_file >$temp_out_file
    else
        paste -d',' $temp_out_file_helper $temp_value_file >$temp_out_file
    fi

    mv $temp_out_file $temp_out_file_helper
    rm $temp_value_file
done

sed 's/^/(/' $temp_out_file_helper | sed 's/$/),/' | sed '$ s/,$/ ON CONFLICT DO NOTHING;/' >$temp_out_file

echo -e "start transaction;\n" >$output_file
echo "insert into" $table_name >>$output_file
echo "("$field_names")" >>$output_file
echo "values" >>$output_file

cat $temp_out_file | sed 's/,,/,null,/g' | sed 's/,)/,null)/g' >>$output_file

echo -e "\ncommit;" >>$output_file

rm $temp_out_file
rm $temp_out_file_helper
