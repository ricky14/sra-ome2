
folder='txt_for_sql'
out_dir='txt_for_sql'
all_data_file_f=$1

#---------------------------------------------------------------------------

#STUDYSAMPLE
#(StudyAccession, SampleAccession)
#PK: (StudyAccession, SampleAccession)

study_pk_file=$folder'/Study_0_PK.txt'
sample_pk_file=$folder'/Sample_0_PK.txt'

studysample_pk_file_non_uniq=$folder'/StudySample_0_PK.txt'
studysample_pk_file_uniq=$folder'/StudySample_0_PK_uniq.txt'

echo paste
#create association
paste -d',' $study_pk_file  $sample_pk_file \
>$studysample_pk_file_non_uniq

echo unique
#take uniq rows

#cat $studysample_pk_file \
#| sort  \
#| sort +2  \
#| sort -u  \
#>$studysample_pk_file_uniq

length=$(wc -l $studysample_pk_file_non_uniq | awk '{print $1;}')
#echo $length
[ -e temp.txt ] && rm temp.txt
bloc=1000
for ((i=0; i*bloc<length; i++))
do
    #echo $i
    sed -n "$((i*bloc+1)),$(((i+1)*bloc))p;$(((i+1)*bloc+1))q" $studysample_pk_file_non_uniq \
    | sort \
    >> temp.txt
done

#wc -l temp.txt
cat temp.txt \
| sort -u \
>$studysample_pk_file_uniq

rm temp.txt

echo cat study
#take Study from uniq columns
cat $studysample_pk_file_uniq \
| cut -d',' -f1 \
>$folder'/StudySample_0_PK1_Study_uniq.txt' 

echo cat sample
#take Sample from uniq columns
cat $studysample_pk_file_uniq \
| cut -d',' -f2 \
>$folder'/StudySample_0_PK2_Sample_uniq.txt' 

./create_insert_into_sql_from_columns.sh StudySample.sql StudySample \
StudyAccession,SampleAccession \
$out_dir/StudySample_0_PK1_Study_uniq.txt,$out_dir/StudySample_0_PK2_Sample_uniq.txt