#set -x

nOfParamsNeeded=5

if test $# -lt $nOfParamsNeeded
then
	echo "usage: $0 <input_file> <field_header_container> <field_header_regex> <field_trailer_regex> <field_trailer_container>"
	echo "example: $0"' experiment_formatted.xml <EXPERIMENT accession=" " >'
	exit 1
fi

input_file="$1"
field_container="$2"
field_header="$3"
field_trailer="$4"
field_end="$5"


#implementation:
#pros:
#	very efficient
#cons:
#	works well only in field searched is in relation (1,1) with ExperimentAccession
#	because 1): don't know where found line (problems with nullable fields)
#	because 2): find and saves all grepping results
#'s/[\s\S]*?(?=<SAMPLE)//g'
searched_value=`cat "$input_file" \
| sed -e "s/.*$field_container/$field_container/g" \
| sed s:"$field_end".*:: \
| sed s/.*"$field_header"//g \
| sed s:"$field_trailer".*::\
| sed "s:$field_container.*::"`

if test -z "$searched_value"
then
	#error not found
	#echo "error: $input_file, $field_header, $field_trailer" >&2
	echo ""
else
	#ok
	echo "$searched_value"
fi