#!/bin/bash

scriptBase='./getAllFieldValues_with_container.sh'
folder='./csv_pick_all'
out_dir='./csv_pick_all'
all_data_file_f=$1

#---------------------------------------------------------------------------

cp $all_data_file_f ./data/
cd ./data/
name=`echo -n "$all_data_file_f" | sed "s/^.*\///" | sed "s/_fetch.xml//"`
./format_fetch_all.sh "$name" "or"

cd ..
all_data_file_f="./data/"$name"_fetch_rows.xml"




echo PK
#create pk file
#useful for other scripts, not useful for db insert
$scriptBase \
$all_data_file_f \
'<RUN ' \
'accession="' \
'"' \
'>' \
>$out_dir/Run_0_PK.txt

echo SRAFileSizeMB
#Run -> SRAFileSizeMB
$scriptBase \
$all_data_file_f \
'<RUN ' \
'size="' \
'"' \
'>' \
>$out_dir'/Run_2_SRAFileSizeMB_temp.txt'

echo -n "" > $out_dir'/Run_2_SRAFileSizeMB.txt' 
while read size_byte;
do
    echo $(($size_byte/1048576)) >> $out_dir'/Run_2_SRAFileSizeMB.txt' 
done < $out_dir'/Run_2_SRAFileSizeMB_temp.txt'

rm $out_dir'/Run_2_SRAFileSizeMB_temp.txt' 

echo LibraryLayout
#Experiment -> LibraryLayout
$scriptBase \
$all_data_file_f \
'<EXPERIMENT ' \
'<LIBRARY_LAYOUT><' \
'/></LIBRARY_LAYOUT>' \
'</EXPERIMENT>' \
> $out_dir/'Experiment_7_LibraryLayout.txt'


./create_insert_into_sql_from_columns.sh $out_dir/run_info.sql Run \
run,size,layout, \
$out_dir/Run_0_PK.txt,$out_dir/Run_2_SRAFileSizeMB.txt,$out_dir/Experiment_7_LibraryLayout.txt

echo "Run,size_MB,LibraryLayout" > $out_dir/run_info.csv

cat $out_dir/run_info.sql \
| grep "(E" \
| sed "s/',E'/,/g" \
| sed "s/(E'//" \
| sed "s/').*//" \
| sed "s/SINGLE.*/SINGLE/" \
| sed "s/PAIRED.*/PAIRED/" \
>> $out_dir/run_info.csv

rm $out_dir/run_info.sql
rm $out_dir/Run_0_PK.txt
rm $out_dir/Run_2_SRAFileSizeMB.txt
rm $out_dir/Experiment_7_LibraryLayout.txt
rm ./data/*.xml