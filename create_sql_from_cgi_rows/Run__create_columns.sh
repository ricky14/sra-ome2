
scriptBase='./getAllFieldValues_with_container.sh'
folder='txt_for_sql'
out_dir='txt_for_sql'
all_data_file_f=$1

#---------------------------------------------------------------------------

#RUN
#(RunAccession, PublicationDateTime, SRAFileSizeMB, FASTQFileSizeMB, Spot, Base,
#   Consent, Note, RunOutcome, ExperimentAccession)
#PK: RunAccession

#./getAllFieldValuesWithRegionsAndIdentifier.sh 'txt_for_sql/Run_0_PK.txt' 'txt_for_sql/1_regions_for_run.txt' 'experiment_formatted.xml'

#NB: FASTQFileSizeMB: not known from metadata, 
#   default: null
#NB: RunOutcome: not known from metadata, 
#   default: if consent = 'public' or is null -> TO_DO, else -> IGNORE 
#NB: Note default null

#NB metadata is grouped by experiments (why sra?) which are less than runs
#so we cannot use the region file used till now (was a delimiter for experiments)
#we will use region file for runs
#to generate it:
#cat $all_data_file \
#| grep -n '</RUN>' \
#| cut -d':' -f1 >$region_file

run_pk_file_non_uniq=$folder'/Run_0_PK.txt'
run_pk_file_uniq=$folder'/Run_0_PK_sort.txt'

echo PK
#create pk file
#useful for other scripts, not useful for db insert
$scriptBase \
$all_data_file_f \
'<RUN ' \
'accession="' \
'"' \
'>' \
>$run_pk_file_non_uniq

echo RunAccession
#take uniq pk file
#Run -> RunAccession
cat $run_pk_file_non_uniq \
| uniq \
| sort \
| uniq \
>$run_pk_file_uniq

echo PublicationDateTime
#Run -> PublicationDateTime
$scriptBase \
$all_data_file_f \
'<RUN ' \
'published="' \
'"' \
'>' \
>$out_dir'/Run_1_PublicationDateTime.txt'

echo SRAFileSizeMB
#Run -> SRAFileSizeMB
$scriptBase \
$all_data_file_f \
'<RUN ' \
'size="' \
'"' \
'>' \
>$out_dir'/Run_2_SRAFileSizeMB.txt'

echo Spot
#Run -> Spot
$scriptBase \
$all_data_file_f \
'<RUN ' \
'total_spots="' \
'"' \
'>' \
>$out_dir'/Run_3_Spot.txt'

echo Base
#Run -> Base
$scriptBase \
$all_data_file_f \
'<RUN ' \
'total_bases="' \
'"' \
'>' \
>$out_dir'/Run_4_Base.txt'

echo Consent
#Run -> Consent
$scriptBase \
$all_data_file_f \
'<RUN ' \
'cluster_name="' \
'"' \
'>' \
>$out_dir'/Run_5_Consent.txt'

echo ExperimentAccession
#Run -> ExperimentAccession
$scriptBase \
$all_data_file_f \
'<RUN ' \
'<EXPERIMENT_REF (accession|refname)="' \
'"' \
'</RUN>' \
>$out_dir'/Run_6_ExperimentAccession.txt'

#Run -> ExperimentAccession
#dal xml è molto (troppo) complicato trovare l'esperimento di ogni run
#questo perchè è un rapporto uno-molti:
#<EXPERIMENT ....>
# ...
#<RUN_SET>
#   <RUN  1 ...>
#   </RUN 1 ...>
#   <RUN  2 ...>
#   </RUN 2 ...>
#   ...
#</RUN_SET>
#...
#</EXPERIMENT ....>

#conviene usare il csv (ottenuto scaricando il file con runinfo)
#run: campo 1
#exp: campo 11
#cat metadata_original.csv \
#| cut -d',' -f1,11 \
#| grep '^[DES]RR[0-9]*,[DES]RX[0-9]*$' \
#| sort \
#| cut -d',' -f2 \
#>$out_dir'/Run_6_ExperimentAccession.txt'

./create_insert_into_sql_from_columns.sh Run.sql Run \
RunAccession,PublicationDateTime,SRAFileSizeMB,Spot,Base,Consent,ExperimentAccession \
$out_dir/Run_0_PK.txt,$out_dir/Run_1_PublicationDateTime.txt,$out_dir/Run_2_SRAFileSizeMB.txt,$out_dir/Run_3_Spot.txt,$out_dir/Run_4_Base.txt,$out_dir/Run_5_Consent.txt,$out_dir/Run_6_ExperimentAccession.txt