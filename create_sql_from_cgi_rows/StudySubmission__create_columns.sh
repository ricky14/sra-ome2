
out_dir="txt_for_sql"
folder="txt_for_sql"
all_data_file_f=$1

#---------------------------------------------------------------------------

#STUDYSUBMISSION
#(StudyAccession, SubmissionAccession)
#PK: (StudyAccession, SubmissionAccession)

study_pk_file=$out_dir'/Study_0_PK.txt'
submission_pk_file=$out_dir'/Submission_0_PK.txt'

studysubmission_pk_file=$out_dir'/StudySubmission_0_PK.txt'
studysubmission_pk_file_uniq=$out_dir'/StudySubmission_0_PK_uinq.txt'

echo paste
#create association
paste -d',' $study_pk_file  $submission_pk_file \
>$studysubmission_pk_file

echo unique
length=$(wc -l $studysubmission_pk_file | awk '{print $1;}')
#echo $length
[ -e temp.txt ] && rm temp.txt
bloc=1000
for ((i=0; i*bloc<length; i++))
do
    #echo $i
    sed -n "$((i*bloc+1)),$(((i+1)*bloc))p;$(((i+1)*bloc+1))q" $studysubmission_pk_file \
    | sort \
    >> temp.txt
done

cat temp.txt \
| sort -u \
>$studysubmission_pk_file_uniq

rm temp.txt

echo cat study
#take Study from uniq columns
cat $studysubmission_pk_file_uniq \
| cut -d',' -f1 \
>$folder'/StudySubmission_0_PK1_Study_uniq.txt' 

echo cat Submission
#take Submission from uniq columns
cat $studysubmission_pk_file_uniq \
| cut -d',' -f2 \
>$folder'/StudySubmission_0_PK2_Submission_uniq.txt' 


./create_insert_into_sql_from_columns.sh StudySubmission.sql StudySubmission \
StudyAccession,SubmissionAccession \
$out_dir/StudySubmission_0_PK1_Study_uniq.txt,$out_dir/StudySubmission_0_PK2_Submission_uniq.txt