#!/bin/bash

START_TIME=$SECONDS

if [ $# -eq 1 ]
then
    prefix="$1"
    echo $prefix
else
    exit 4
fi

cd ./data
 
status=$?
if [ $status -eq 0 ]
then
    #echo "Submit to format:"
    #read

    ## To create only rows file
    ./format_fetch_all.sh "$prefix" 'or'

    ## To create rows file and formatted
    #./format_fetch_all.sh $prefix 'f'
    
    status=$?
else
    echo "Errore nel download dei dati"
    exit $status
fi
cd ..


if [ $status -eq 0 ]
then
    #echo "Submit for sql:"
    #read

    
    rm ./txt_for_sql/*.txt
    rm ./sql_to_insert/*.sql
    ./create_all_tables_from_rows.sh "$prefix"
    status=$?
    if [ $status -eq 0 ]
    then
        ELAPSED_TIME=$(($SECONDS - $START_TIME))
        echo "Conversione XML -> sql eseguita correttamente in "$ELAPSED_TIME"s"
        rm ./parser_temp/*.txt

        cd ./sql_to_insert
        for file in $(ls)
        do
            cp $file './../../SRA/sql/6_insert_population'
        done
        cd ..


        #./csv_pick_all/create_csv_from_xml.sh ./data/"$prefix"_fetch_rows.xml


        cd ./data
        rm $prefix*.xml
        exit 0
    else
        echo "Errore nella traduzione da XML ad sql"
    fi
else
    echo "Si è verificato un errore nella fromattazione dei dati XML"
fi
