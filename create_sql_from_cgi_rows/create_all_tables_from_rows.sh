
prefix=$1

dataFile(){
    echo './data/'$prefix'_fetch.xml'
}

dataFile_r(){
    echo './data/'$prefix'_fetch_rows.xml'
}
sql_final_dir="./sql_to_insert/"

rm $sql_final_dir*.sql
rm ./txt_for_sql/*.txt

## Experiment table
echo Experiment
./Experiment__create_columns_v2.sh $(dataFile_r)
cat ./Experiment.sql | sed "s/,E'PAIRED .*\"',/,E'PAIRED',/" > $sql_final_dir"4_sra_Experiment.sql"
rm ./Experiment.sql
#mv ./Experiment.sql $sql_final_dir"4_sra_Experiment.sql"  ##Problema in caso di PAIRED con altre info
#wc -l ./txt_for_sql/Experiment_*

## Institution table
echo Institution
./Institution__create_columns.sh $(dataFile_r)
mv ./Institution.sql $sql_final_dir"0_sra_Institution.sql"
#wc -l ./txt_for_sql/Institution_*

## Run table
echo Run
./Run__create_columns.sh $(dataFile_r)
mv ./Run.sql $sql_final_dir"5_sra_Run.sql"
#wc -l ./txt_for_sql/Run_*

## Sample table
echo Sample
./Sample__create_columns.sh $(dataFile_r)
mv ./Sample.sql $sql_final_dir"3_sra_Sample.sql"
#wc -l ./txt_for_sql/Sample_*

## SampleExtra table
echo SampleExtra
./SampleExtra__create_columns.sh $(dataFile_r)
mv ./SampleExtra.sql $sql_final_dir"3_sra_SampleExtra.sql"
#wc -l ./txt_for_sql/Sample_*

## Study table
echo Study
./Study__create_columns.sh  $(dataFile_r)
mv ./Study.sql $sql_final_dir"0_sra_Study.sql"
#wc -l ./txt_for_sql/Study_*

## Submission table
echo Submission
./Submission__create_columns.sh $(dataFile_r)
mv ./Submission.sql $sql_final_dir"0_sra_Submission.sql"
#wc -l ./txt_for_sql/Submission_*

## StudySample
echo StudySample
./StudySample__create_columns.sh $(dataFile_r)
mv ./StudySample.sql $sql_final_dir"4_sra_StudySample.sql"
#wc -l ./txt_for_sql/StudySample*

## StudySubmission
echo StudySubmission
./StudySubmission__create_columns.sh $(dataFile_r)
mv ./StudySubmission.sql $sql_final_dir"1_sra_StudySubmission.sql"
#wc -l ./txt_for_sql/StudySubmission*

## StudySupervisor 
echo StudySupervisor
./StudySupervisor__create_columns_v2.sh $(dataFile_r)
mv ./StudySupervisor.sql $sql_final_dir"1_sra_StudySupervisor.sql"
#wc -l ./txt_for_sql/StudySupervisor*

cd txt_for_sql
rm *
cd ..