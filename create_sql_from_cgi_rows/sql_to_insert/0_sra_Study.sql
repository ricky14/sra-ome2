start transaction;

insert into Study
(StudyAccession,Title,Abstract,BioProjectID)
values
(E'SRP172673',E'AM fungal community associated to Vitis vinifera in a Northern Italy vineyard subjected to integrated pest management',null,E'PRJNA508468'),
(E'SRP172673',E'AM fungal community associated to Vitis vinifera in a Northern Italy vineyard subjected to integrated pest management',null,E'PRJNA508468'),
(E'SRP172673',E'AM fungal community associated to Vitis vinifera in a Northern Italy vineyard subjected to integrated pest management',null,E'PRJNA508468'),
(E'SRP172673',E'AM fungal community associated to Vitis vinifera in a Northern Italy vineyard subjected to integrated pest management',null,E'PRJNA508468') ON CONFLICT DO NOTHING;

commit;
