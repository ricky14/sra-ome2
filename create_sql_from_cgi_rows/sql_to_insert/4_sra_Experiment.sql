start transaction;

insert into Experiment
(ExperimentAccession,Title,Alias,Design,LibraryName,SampleAccession,LibrarySource,LibraryLayout,LibrarySelection,LibraryStrategy,PlatformName,InstrumentName)
values
(E'SRX5099252',E'Metagenome or environmental sample from soil metagenome',E'Bs-1C',E'amplicon',E'Bs-1C',E'SRS4109622',E'METAGENOMIC',E'SINGLE',E'PCR',E'AMPLICON',E'LS454',E'454 GS Junior'),
(E'SRX5099251',E'Metagenome or environmental sample from soil metagenome',E'Bs-2C',E'amplicon',E'Bs-2C',E'SRS4109621',E'METAGENOMIC',E'SINGLE',E'PCR',E'AMPLICON',E'LS454',E'454 GS Junior'),
(E'SRX5099250',E'Metagenome or environmental sample from soil metagenome',E'Rhizo-1C',E'amplicon',E'Rhizo-1C',E'SRS4109620',E'METAGENOMIC',E'SINGLE',E'PCR',E'AMPLICON',E'LS454',E'454 GS Junior'),
(E'SRX5099249',E'Metagenome or environmental sample from soil metagenome',E'Rhizo-2C',E'amplicon',E'Rhizo-2C',E'SRS4109619',E'METAGENOMIC',E'SINGLE',E'PCR',E'AMPLICON',E'LS454',E'454 GS Junior') ON CONFLICT DO NOTHING;

commit;
