start transaction;

insert into StudySample
(StudyAccession,SampleAccession)
values
(E'SRP172673',E'SRS4109619'),
(E'SRP172673',E'SRS4109620'),
(E'SRP172673',E'SRS4109621'),
(E'SRP172673',E'SRS4109622') ON CONFLICT DO NOTHING;

commit;
