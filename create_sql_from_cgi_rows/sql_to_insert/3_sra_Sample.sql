start transaction;

insert into Sample
(SampleAccession,Title,Description,Alias,BioSampleID,TaxonID)
values
(E'SRS4109622',E'soil metagenome',null,E'Bulksoil 1C',E'SAMN10525947',E'410658'),
(E'SRS4109621',E'soil metagenome',null,E'Bulksoil 2C',E'SAMN10525948',E'410658'),
(E'SRS4109620',E'soil metagenome',null,E'Rhizo 1C',E'SAMN10525949',E'410658'),
(E'SRS4109619',E'soil metagenome',null,E'Rhizo 2C',E'SAMN10525950',E'410658') ON CONFLICT DO NOTHING;

commit;
