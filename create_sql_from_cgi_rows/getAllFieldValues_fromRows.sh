#set -x

nOfParamsNeeded=3

if test $# -lt $nOfParamsNeeded
then
	echo "usage: $0 <input_file> <field_header_regex> <field_trailer_regex>"
	echo "example: $0 experiment_formatted.xml '<STUDY_>' '</STUDY_TITLE>'"
	exit 1
fi

input_file="$1"
field_header="$2"
field_trailer="$3"


#implementation:
#pros:
#	very efficient
#cons:
#	works well only in field searched is in relation (1,1) with ExperimentAccession
#	because 1): don't know where found line (problems with nullable fields)
#	because 2): find and saves all grepping results

searched_value=`cat "$input_file" \
| sed s/.*"$field_header"//g \
| sed s:"$field_trailer".*::`

if test -z "$searched_value"
then
	#error not found
	#echo "error: $input_file, $field_header, $field_trailer" >&2
	echo ""
else
	#ok
	echo "$searched_value"
fi