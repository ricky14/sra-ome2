const express = require('express');
const bodyParser = require('body-parser');
const shell = require('shelljs');
const cp = require('child_process');
const fs = require('fs');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.set('view engine', 'ejs');
const port = 8080;

const { Pool, Client } = require('pg');
//const { arch } = require('os');
const fileUpload = require('express-fileupload');
//const { send } = require('process');
const path = require('path');
const { log } = require('console');

const dir_sra="/root/SRA"
const dir_script = "/root/SRA/scripts";
const dir_output_server = "/root/SRA/output_server";
const dir_sql_db = "/root/SRA/sql";
const dir_csv_post = "/root/SRA/app_post_csv";
const dir_xml_post = "/root/SRA/app_post_xml";
const dir_sra_metadata = "/root/SRA/metadata";
const dir_sra_reports = "/root/SRA/reports";
const dir_to_bash_for_xml="/root/create_sql_from_cgi";

String.prototype.replaceAt = function(index, replacement) {
  return this.substring(0, index) + replacement + this.substring(index + replacement.length);
}



app.get('/hello', (req, res) => {
  console.log("Accesso da " + req.ip);
  res.send('Benvenuto nel mio tirocinio!\n');
});

app.get('/pwd', (req, res) => {
  console.log("Accesso da "+req.ip);
  res.send(shell.pwd().stdout+'\n');
  /*
  var yourscript = cp.exec(dir_script + '/god_pwd.sh',
    (error, stdout, stderr) => {
      console.log(stdout);
      console.log(stderr);
      if (error !== null) {
        console.log(`exec error: ${error}`);
        res.send(stderr + "\n");
      }
      res.send(stdout + "\n");
    });/**/
});

app.get('/cd', (req, res) => {
  console.log("Accesso da " + req.ip);
  let path = req.query.path;
  res.send(shell.cd(path).stdout + '\n');
});

app.get('/exec_obj', (req, res) => {
  console.log("Accesso da " + req.ip);
  console.log(shell.exec('cd'))
  res.send(shell.exec('cd').code + '\n');
});

app.get('/exec', (req, res) => {
  console.log("Accesso da " + req.ip);
  let cmd = req.query.cmd;
  res.send(shell.exec(cmd).stdout + '\n');
});








//########################## Database starting operations #####################################################################

/*
app.get('/load_db', (req, res) => {

  console.log("Accesso da " + req.ip);

  let scrittura_db_e_output = new Promise(function (resolve, reject) {
    try {
      console.log("Scrittura dati nel DB");
      var fd = fs.openSync(dir_output_server + '/output_load_db.txt', 'w');
      var data = shell.exec(dir_sql_db + '/general_populate.sh ').stdout;
      fs.writeFileSync(
        fd,
        data,
        'utf8'
      );
      fs.closeSync(fd);
      console.log("DB caricato");
      resolve("Il DB e' stato inizializzato");

    } catch (error) {
      console.error(error);
      reject(error);
    }
  });

  scrittura_db_e_output
    .then((result) => { res.send(result + "\n"); })
    .catch((result) => { res.send("Si e' verificato un errore:\n" + result + "\n"); });

});/**/

app.get('/load_tax_krak', (req, res) => {

  console.log("Accesso da " + req.ip);
  
  var sql_files = shell.ls(dir_sql_db + '/4_population').stdout;
  sql_files = sql_files.split('\n');
  var i = 0;
  const client = new Client()
  try{
  client.connect()
    .then(() => {
      promise_list = []
      sql_files.forEach(file_name => {
        if (file_name != '') {
          var sql_text = fs.readFileSync(dir_sql_db + '/4_population/' + file_name).toString();
          let name_activity = file_name+"_"+req.ip+"_"+Date.now();
          insert_new_activity(name_activity, "open");
          promise_list[i] = client.query(sql_text).then(() => {
            console.log("File " + file_name + " esguito");
            //update_activity(name_activity, "closed");
            remove_activity(name_activity);
          })
          .catch((error)=>{
            update_activity(name_activity, "error", "{"+error+"}");
          });
          i++;
        }
      });
      console.log("File da eseguire: " + i);
      Promise.all(promise_list).then((ris) => {
        res.send("All files have been uploaded :\n" + ris + "\n");
        client.end();
      })
      .catch((error) => {
        console.log(error);
        client.end();
        res.send("Erron on sql file:\n" + error + "\n");
      })
    })
    .catch(() => {
      res.send("Erroron DB connection\n");
    });
  }catch(error){
    res.send("Error on:\n"+error+"\n");
  }
});

/*
app.get('/reboot_db', (req, res) => {

  console.log("Accesso da " + req.ip);
  fs.writeFileSync(dir_output_server + '/output_reboot_db.txt', "Accesso da " + req.ip + "\n", err => {
    if (err) {
      console.error(err);
    }
    // done!
  });
  const client = new Client()
  client.connect().then(() => {
    sql_text=fs.readFileSync(dir_sql_db + '/0_reboot.sql').toString();
    client.query(sql_text).then(() => {
      console.log("Reboot esguito");
      res.send("")
      fs.appendFileSync(dir_output_server + '/output_reboot_db.txt', "Reboot esguito\n", err => {
        if (err) {
          console.error(err);
        }
        // done!
      });
    }).catch((errore)=>{
      res.send("Errore nel reboot:\n"+errore+'\n');
    })
  }).catch((error) => {
    res.send("Errore nella connessione al DB:\n"+error+'\n');
  });

});/**/


function exec_insert_sql_file_from_directory(directory_path){
  //let now = (new Date(Date.now())).toISOString();
  let act_insert_name = "Insert_"+Date.now();
  insert_new_activity(act_insert_name, "inserting", "{path: "+directory_path+"}");

  var sql_files = shell.ls(directory_path).stdout;
  console.log("File: "+sql_files);
  sql_files = sql_files.split('\n');
  var i = 0;
  const client = new Client()
  client.connect()
    .then(() => {
      promise_list = [];
      sql_files.forEach(file_name => {
        if (file_name != '') {
          var sql_text = fs.readFileSync(directory_path + "/" + file_name).toString();
          let activity_name=file_name+"_"+Date.now();
          insert_new_activity(activity_name, "started");
          promise_list[i] = new Promise((resolve, reject)=>{
            try{
              client.query(sql_text).then(() => {
                console.log("File " + file_name + " esguito");
                //update_activity(activity_name, "done");
                remove_activity(activity_name);
                resolve(file_name + " ok");
              })
              .catch((error)=>{
                update_activity(activity_name, "error", "{"+error+"}");
                reject("File "+file_name +": "+error);
              })
            }catch(error){
              reject(error);
            }
          });
          i++;
        }
      }); 
      console.log("File da eseguire: " + i);
      Promise.all(promise_list)
        .then((values) => {
          client.end();
          //remove_activity(act_insert_name);
          let last_dir = directory_path;
          let index = last_dir.indexOf("/");
          while(index>=0){
            last_dir=last_dir.substring(index+1);
            index=last_dir.indexOf("/");
          }
          update_activity(act_insert_name, "done", "{path: "+last_dir+"}");
          return "Ok";
        })
        .catch((error) => {
          client.end();
          update_activity(act_insert_name, "error", "{"+error+"}");
          return error;
        })
    })
    .catch(() => {
      return "Failed to connect to th db";
    });
    return act_insert_name;
}

app.get('/insert_metadata', (req, res) => {
  var sql_files = shell.ls(dir_sql_db + '/6_insert_population').stdout;
  sql_files = sql_files.split('\n');
  var i = 0;
  const client = new Client()
  client.connect()
    .then(() => {
      promise_list = []
      sql_files.forEach(file_name => {
        if (file_name != '') {
          var sql_text = fs.readFileSync(dir_sql_db + '/6_insert_population/' + file_name).toString();
          let activity_name=file_name+"_"+Date.now()+"_"+req.ip;
          insert_new_activity(activity_name, "started");
          promise_list[i] = new Promise((resolve, reject)=>{
            try{
              client.query(sql_text).then(() => {
                console.log("File " + file_name + " esguito");
                //update_activity(activity_name, "done");
                remove_activity(activity_name);
                resolve(file_name + " ok");
              })
              .catch((error)=>{
                update_activity(activity_name, "error", "{"+error+"}");
                reject("File "+file_name +": "+error);
              })
            }catch(error){
              reject(error);
            }
          });
          i++;
        }
      }); 
      console.log("File da eseguire: " + i);
      Promise.all(promise_list)
        .then((values) => {
          client.end();
          res.send("All files have been uploaded\n"+values+"\n");
        })
        .catch((error) => {
          client.end();
          res.send("Error on DB connection:\n"+error+"\n");
        })
    })
    .catch(() => {
      res.send("Error on DB connection\n");
    });


});

app.get('/insert_functions', (req, res) => {
  var sql_files = shell.ls(dir_sql_db + '/5_functions').stdout;
  sql_files = sql_files.split('\n');
  var i = 0;
  const client = new Client()
  client.connect()
    .then(() => {
      promise_list = []
      sql_files.forEach(file_name => {
        if (file_name != '') {
          var sql_text = fs.readFileSync(dir_sql_db + '/5_functions/' + file_name).toString();
          let activity_name=file_name+"_"+Date.now()+"_"+req.ip;
          insert_new_activity(activity_name, "started");
          promise_list[i] = new Promise((resolve, reject)=>{
            try{
              client.query(sql_text).then(() => {
                console.log("File " + file_name + " esguito");
                //update_activity(activity_name, "done");
                remove_activity(activity_name);
                resolve(file_name + " ok");
              })
              .catch((error)=>{
                update_activity(activity_name, "error", "{"+error+"}");
                reject("File "+file_name +": "+error);
              })
            }catch(error){
              reject(error);
            }
          });
          i++;
        }
      }); 
      console.log("File da eseguire: " + i);
      Promise.all(promise_list)
        .then((values) => {
          client.end();
          res.send("All files have been uploaded\n"+values+"\n");
        })
        .catch((error) => {
          client.end();
          res.send("Error on sql file:\n"+error+"\n");
        })
    })
    .catch(() => {
      res.send("Error on DB connection\n");
    });


});

app.get('/reboot_db', (req, res) => {
  console.log("Accesso da " + req.ip);

  let scrittura_db_e_output = new Promise(function (resolve, reject) {
    try {
      console.log("Reboot del DB");
      var fd = fs.openSync(dir_output_server + '/output_drop_db.txt', 'w');
      var data = shell.exec(dir_sql_db + '/reboot.sh ').stdout;
      fs.writeFileSync(
        fd,
        data,
        'utf8'
      );
      fs.closeSync(fd);
      console.log("DB creato");
      reset_activity();
      resolve("Il DB é stato ricreato");

    } catch (error) {
      console.error(error);
      reject(error);
    }
  });

  scrittura_db_e_output
    .then((result) => { res.send(result + "\n"); })
    .catch((result) => { res.send("Error on:\n" + result + "\n"); });

});

app.get('/create_kraken_sql', (req, res) =>{
  console.log("Accesso da " + req.ip);
  let inspect_url=req.query.url
  let collection_date=req.query.date
  let archive_size=req.query.archive_size
  let index_size=req.query.index_size
  shell.cd('/root/SRA/sql/create_sql_files_to_populate_db/kraken2_taxonomy/');
  let instr = "./create_Standard16_Kraken_tabels_sql_files.sh '"+inspect_url+"' '"+collection_date+"' '"+archive_size+"' '"+index_size+"'";
  res.send(shell.exec(instr).stdout);
});

app.get('/create_taxon_sql', (req, res) =>{
  console.log("Accesso da " + req.ip);
  let url=req.query.url
  let collection=req.query.name
  let collection_date=req.query.date
  shell.cd('/root/SRA/sql/create_sql_files_to_populate_db/ncbi_taxonomy/');
  let instr = "./download_and_create.sh '"+url+"' '"+collection+"' '"+collection_date+"'";
  //res.send(shell.exec(instr).stdout);
  shell.exec(instr, function(code, stdout, stderr) {
    res.send(code+"\n");
    //console.log('Program output:', stdout);
    console.log('Program stderr:', stderr);
    console.log('Exit code:', code);
  });
});




 

//########################## Async activity database #####################################################################

function check_data(data){
  if(data.indexOf('{')!=0){
    return "{}"
  }else{
    return data
  }
}

function insert_new_activity(name, status, data="{}"){
  data=check_data(data);
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  if(data==""){
    data="{}";
  }
  let output = shell.exec("./insert_new_activity.sh '"+name+"' '"+status+"' '"+data+"'").stdout;
  return output;
}

function update_activity(name, status, data=""){
  data=check_data(data);
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  let exit_code
  if(data==""){
    exit_code = shell.exec(dir_db_activity+"update_activity.sh '"+name+"' '"+status+"'").code;
  }else{
    exit_code = shell.exec(dir_db_activity+"update_activity.sh '"+name+"' '"+status+"' '"+data+"'").code;
  }
  
  return exit_code;
}

function remove_activity(name){
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  let exit_code = shell.exec(dir_db_activity+"remove_activity.sh '"+name+"'").code;
  return exit_code;
}

function remove_ended_activity(){
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  let exit_code = shell.exec(dir_db_activity+"remove_ended_activity.sh").code;
  return exit_code;
}

function reset_activity(){
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  let exit_code = shell.exec(dir_db_activity+"remove_ended_activity.sh 1").code;
  return exit_code;
}

function defram_activity(){
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  let exit_code = shell.exec(dir_db_activity+"defram_activity.sh").code;
  return exit_code;
}

function get_all_activity(){
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  let json = shell.exec(dir_db_activity+"get_all_activity.sh").stdout;
  return json;
}

function get_activity_status(name){
  let dir_db_activity=dir_output_server+"/db_activity/"
  shell.cd(dir_db_activity);
  let status = shell.exec(dir_db_activity+"get_activity_status_by_name.sh '"+name+"'").stdout;
  status = status.substring(0, status.length-1);
  return status;
}

function get_activity_list(){
  let list_act = [];
  let rows = shell.cat(dir_output_server+"/db_activity/activity.txt").stdout.split('\n');
  rows.pop();
  rows.forEach(row =>{
    let act = [];
    let row_s = row.split(',');
    act.push(row_s[0]);
    act.push(row_s[1]);
    act.push(row_s[2]);
    let start = row.indexOf('{');
    if(start > 0){
      act.push(row.substring(start));
    }else{
      act.push("");
    }
    list_act.push(act);
  })
  return list_act;
}



app.get('/activity/get_all', (req, res)=>{
  res.send(get_all_activity());
});

app.get('/activity/reset', (req, res)=>{
  res.send(""+reset_activity());
});

app.get('/activity/clear_closed', (req, res)=>{
  res.send(""+remove_ended_activity());
});

app.get('/activity/insert', (req, res)=>{
  let name = req.query.name;
  let data = req.query.data;
  if(name==undefined){
    res.send("No name found on query\n");
  }else{
    let code;
    if(data==undefined){
      code = insert_new_activity(name, "open");
    }else{
      code = insert_new_activity(name, "open",data);
    }
    res.send("Activity: '"+name+"' code "+code);
  }
});

app.get('/activity/test', (req,res)=>{
  remove_activity("panda2")
  insert_new_activity("panda","open");
  cp.execSync('sleep 5');
  insert_new_activity("panda2", "open", "{helloooo}");
  cp.execSync('sleep 5');
  update_activity("panda", "closed", "{bye, byeee}");
  cp.execSync('sleep 5');
  remove_activity("panda");
  cp.execSync('sleep 5');
  defram_activity();
  remove_activity("panda2");
  res.send("Finished\n");
});









//########################## csv and god endpoint #####################################################################

app.get('/csv_files', (req, res)=>{
  let list = [];
  list = (shell.exec("ls "+dir_csv_post+" | egrep -v '_god_split_[0-9]*.csv'").stdout).split('\n');
  list.pop();
  res.render(__dirname+"/views/csv_list.ejs", {files:list});
})

app.get('/csv_to_see', (req, res)=>{
  if(shell.test('-e', dir_csv_post+"/"+req.query.name)){
    res.sendFile(dir_csv_post+"/"+req.query.name);
    
  }else{
    res.send('File not found');
  }
    
});

app.get('/csv_to_download', (req, res)=>{
  if(shell.test('-e', dir_csv_post+"/"+req.query.name)){
    res.download(dir_csv_post+"/"+req.query.name);
    
  }else{
    res.send('File not found');
  }
    
});

app.post('/csv_todo', (req, res)=>{
  let file_name=dir_csv_post+"/"+req.body.name;
  let act_name = "God_"+req.body.name+"_"+Date.now();
  //console.log(req.body);
  if(file_name == dir_csv_post+"/undefined"){
    res.send("No name found on query");
  }else{
    if(shell.test('-e', file_name)){
      insert_new_activity(act_name, "todo");
      res.send("Activity "+act_name+" created, go to the <a href='/activity_list'>list</a> to check the execution");
    }else{
      res.send("Error: file does not exist");
    }
  
  }
})

app.post('/csv_to_remove', (req, res)=>{
  let file_name=dir_csv_post+"/"+req.body.name;
  //console.log(req.body);
  if(file_name == dir_csv_post+"/undefined"){
    res.send("No name found on query");
  }else{
    if(shell.test('-e', file_name)){
      shell.exec("rm '"+file_name+"'");
      res.redirect("/csv_files");
    }else{
      res.send("Error: file does not exist");
    }
  
  }
})

app.post('/csv_todo_split', (req, res)=>{
  let file_name=dir_csv_post+"/"+req.body.name;
  //console.log(req.body);
  if(file_name == dir_csv_post+"/undefined"){
    res.send("No name found on query");
  }else{
    if(shell.test('-e', file_name)){
      shell.cd(dir_sra);
      shell.exec("./csv_split_n.sh '"+file_name+"' "+req.body.parts_num);
      shell.cd(dir_csv_post);
      let cmd = "ls '"+dir_csv_post+"' | egrep '"+req.body.name.substring(0, req.body.name.length-4)+"_god_split_[0-9]*.csv'";
      console.log(cmd);
      let files = shell.exec(cmd).stdout.split("\n");
      files.pop();
      console.log(files);
      files.forEach(name=>{
        let index = name.indexOf(req.body.name);
        let act_name = "God_"+name.substring(index)+"_"+Date.now();
        insert_new_activity(act_name, "todo");
      })
      
      res.send("Activities for the file "+req.body.name+" rows created, go to the <a href='/activity_list'>list</a> to check the execution");
    }else{
      res.send("Error: file does not exist");
    }
  
  }
})

app.post('/upload_only_csv', 
  fileUpload({createParentPath: true}),
  (req, res)=>{
    const files = req.files;
    console.log(files);
    let prefix = req.body.analysis_name;

    if(prefix=="undefined"){
      res.send("Error: no analysis name found");
    }else{
      let index = prefix.indexOf(" ");
      while(index>=0){
        prefix = prefix.replaceAt(index, "_");
        index = prefix.indexOf(" ");
      }
      index = prefix.indexOf("/");
      while(index>=0){
        prefix = prefix.replaceAt(index, "");
        index = prefix.indexOf("/");
      }
    }
    console.log("Prefix: "+prefix);
    Object.keys(files).forEach(key =>{
      filepath = path.join(dir_csv_post, prefix+"_edit_run_info.csv");
      
      files[key].mv(filepath, (error)=>{
        if (error){ 
          res.send(error);
        }else{
          res.send("Upload successfully done, go to the <a href='/csv_files'>CSV file page</a>");
        }
      })
    })
    
  }

);

app.get('/activity_list', (req, res)=>{
  let list_act = get_activity_list();
  //res.json(list_act)
  res.render(__dirname+"/views/activity_list.ejs", {acts:list_act});
})

app.post('/activity_remove_ended', (req, res)=>{
  remove_ended_activity();
  defram_activity();
  res.redirect("/activity_list");
})

app.post('/insert_new_activity',(req, res)=>{
  let name = req.body.name;
  let status = req.body.status;
  let data = req.body.data;
  insert_new_activity(name, status, data);
  res.redirect('/activity_list');

})

app.post('/update_activity',(req, res)=>{
  let name = req.body.name;
  let status = req.body.status;
  let data = req.body.data;
  update_activity(name, status, data);
  res.redirect('/activity_list');

})

app.post('/remove_one_activity',(req, res)=>{
  let name = req.body.name;
  remove_activity(name);
  if(name.indexOf("_god_split_")>0){
    name = name.substring(4, name.indexOf(".csv")+4);
    if(shell.test("-e", dir_csv_post+"/"+name)){
      shell.exec("rm "+dir_csv_post+"/"+name);
    }
  }
  res.redirect('/activity_list');

});

function select_csv_file_for_god(name){
  let file_path = dir_csv_post+"/"+name;
  if(shell.test('-e', file_path)){
    shell.rm(dir_sra_metadata+"/metadata_filtered_small*");
    let out = shell.exec("cp " + file_path +" "+ dir_sra_metadata+"/metadata_filtered_small_todo.csv").code;
    return out;
  }else{
    return -1;
  }
}

function start_god_activity(){
  let list = get_activity_list();
  let act = [0,];
  let found_todo = false;
  let found_working = false;
  list.forEach(element => {
    if(element[1].indexOf("God_")>=0){
      if(element[2]=="todo" & !found_todo){
        act = element;
        found_todo=true;
      }
      found_working = found_working | (element[2]=="working");
    }
  });
  if(!found_working & found_todo){
    god_execution_with_promise(act);
    return [0, "God started"];
  }else{
    if(found_working){
      return [0, "God is already working"];
    }else{
      shell.cd(dir_script);
      shell.exec("./fix_splitted_report.sh '"+dir_sra+"/reports_info'");
      return [-1, "Not found activity to do"];
    }
  }
}

function god_execution_with_promise(act){
  let file_name=shell.exec("echo -n '"+act[1]+"' | sed 's/^.*God_//' | sed 's/\.csv_.*$/\.csv/'").stdout;
  console.log("File name: "+file_name);

  let selection = new Promise(function(resolve, reject){
    if(select_csv_file_for_god(file_name)==0){
      resolve("Selected");
    }else{
      reject("File \""+file_name+"\" not found");
    }
  })

  selection
    .then((value)=>{
      console.log(value);
      let act_name=act[1];
      update_activity(act_name,"working");
      shell.cd(dir_script);
      //let execution = shell.exec("./god.sh '"+dir_sra_reports+"/"+act_name+"'", {async:true}, 
      //let cmd = "./god.sh";
      //if(file_name.indexOf("_run_info_cgf_.csv")>0 | file_name.indexOf("_run_info_cgf__god_split_")>0){
      //  cmd="./god_for_csv_created.sh";
      //}
      let cmd="./god_for_csv_created.sh";
      let execution = shell.exec(cmd, {async:true}, 
      (code, stdout, stderr)=>{
        if(code==0){
          
          update_activity(act_name, "done");
          //remove_activity(act_name);
          //shell.exec("rm "+dir_csv_post+"/"+file_name);
          shell.exec("mv "+dir_sra_reports+"/* "+dir_sra+"/reports_to_insert/");
          shell.cd(dir_sra+"/MyKrakenThings");
          let out = shell.exec("./convert_all_and_move.sh '"+file_name+"'").code;
          if(out==0){
            exec_insert_sql_file_from_directory(dir_sql_db + '/7_kraken_reports');
          }
          if(act_name.indexOf("_god_split_")>0){
            let file_to_remove = act_name.substring(4, act_name.indexOf(".csv")+4);
            shell.exec("rm '"+dir_csv_post+"/"+file_to_remove+"'");
          }
          start_god_activity();
        }else{
          console.log("Errore nel god");
          update_activity(act_name, "error", "{code:"+code+", err:"+stderr+"}");
        }
        
      });
      
    })
    .catch((value)=>{
      console.log(value);
      update_activity(act[1], "error", "{"+value+"}");
    })

}

app.post('/exec_god', (req, res)=>{
  let out = start_god_activity();
  console.log("Codice "+out[0]+", Output: "+out[1]);
  res.redirect('/activity_list');
})
 

/*
app.post('/csv_move_to_god', (req, res)=>{
  let file_name=dir_csv_post+"/"+req.body.name;
  console.log(req.body);
  if(file_name == dir_csv_post+"/undefined"){
    res.send("No name found on query");
  }else{
    shell.rm(dir_sra_metadata+"/metadata_filtered_small*");
    let out = shell.exec("cp " + file_name +" "+ dir_sra_metadata+"/metadata_filtered_small_todo.csv");
    if(out.code==0){
      shell.exec("echo -n "+req.body.name+" > "+dir_sra_metadata+"/file_name.txt");
      res.send("File uploaded");
    }else{
      res.send("Output: "+out.stderr);
    }
    
  }
  
})


app.get('/exec_god', (req, res)=>{
  let name = shell.cat(dir_sra_metadata+"/file_name.txt").stdout;
  let act_name = "God_"+name+"_"+Date.now();
  insert_new_activity(act_name,"operating");
  shell.cd(dir_script);
  let execution = shell.exec("./god.sh", {async:true}, 
  (code, stdout, stderr)=>{
    if(code==0){
      update_activity(act_name, "closed");
    }else{
      update_activity(act_name, "error:"+code);
    }
  });
  res.send("God is executing, check activity \""+act_name+"\" for updates");
})*/














//########################## Metadata, kraken and report endpoint #####################################################################

app.get('/kraken_sql_gui', (req, res)=>{
  if(req.query.layout == undefined){
    res.render(__dirname+"/views/kraken_psql.ejs", {layout:"create"});
  }else{
    res.render(__dirname+"/views/kraken_psql.ejs", {layout:req.query.layout});
  }
  
})

app.post('/create_report_sql', (req, res)=>{
  shell.cd(dir_sra+"/MyKrakenThings");
  let out = shell.exec("./convert_all_and_move.sh").code;
  if(out==0){
    res.redirect('/kraken_sql_gui?layout=insert');
  }else{
    res.redirect('/kraken_sql_gui?layout=create');
  }
  
});

app.post('/insert_report_sql', (req, res)=>{
  var sql_files = shell.ls(dir_sql_db + '/7_kraken_reports').stdout;
  sql_files = sql_files.split('\n');
  var i = 0;
  const client = new Client()
  client.connect()
    .then(() => {
      promise_list = []
      sql_files.forEach(file_name => {
        if (file_name != '') {
          var sql_text = fs.readFileSync(dir_sql_db + '/7_kraken_reports/' + file_name).toString();
          let activity_name=req.ip+"_"+Date.now()+"_"+file_name;
          insert_new_activity(activity_name, "started");
          promise_list[i] = new Promise((resolve, reject)=>{
            try{
              client.query(sql_text).then(() => {
                console.log("File " + file_name + " esguito");
                //update_activity(activity_name, "done");
                remove_activity(activity_name);
                resolve(file_name + " ok");
              })
              .catch((error)=>{
                update_activity(activity_name, "error");
                reject("File "+file_name +": "+error);
              })
            }catch(error){
              reject(error);
            }
          });
          i++;
        }
      }); 
      console.log("File da eseguire: " + i);
      Promise.all(promise_list)
        .then((values) => {
          client.end();
          res.send("All files have been uploaded\n"+values+"\n");
        })
        .catch((error) => {
          client.end();
          res.send("Error on sql file:\n"+error+"\n");
        })
    })
    .catch(() => {
      res.send("Error on DB connection\n");
    });

});

function convert_and_insert_metadata(prefix){
  let file_name=prefix+"_fetch.xml";
  let act_name="Metadata_conversion_"+file_name+"_"+Date.now();
  insert_new_activity(act_name, "converting", "{}");

  if(shell.test('-e', dir_xml_post+"/"+file_name)){
    shell.cd(dir_to_bash_for_xml);
    shell.exec("cp '"+dir_xml_post+"/"+file_name+"' './data/"+file_name+"'");
    let conversion = new Promise((resolve, reject)=>{
      try{
        let out = shell.exec("./god_convert_user_file.sh '"+prefix+"'").code;
        if(out==0){
          remove_activity(act_name);
          resolve("Conversion ok");
        }else{
          update_activity(act_name, "Conversion failed", "{code:"+out+"}");
          reject("Conversion failed");
        }
      }catch(error){
        update_activity(act_name, "error", "{"+error+"}");
        reject(error);
      }
    });

    conversion
      .then((value)=>{
        let out = exec_insert_sql_file_from_directory(dir_sql_db+"/6_insert_population")
        shell.exec("rm "+dir_xml_post+"/"+prefix+"_fetch.xml");
        console.log("Out then: "+out);
      })
      .catch((value)=>{
        console.log("Error conversion: "+ value);
      })

  }else{
    update_activity(act_name, "error", "{File not found}");
  }
}

function create_csv_from_uploaded_xml(prefix){
  let xml_file_name=prefix+"_fetch.xml";
  let csv_file_name=prefix+"_run_info_cgf_.csv";

  shell.cd(dir_to_bash_for_xml);
  shell.exec("./csv_pick_all/create_csv_from_xml.sh '"+dir_xml_post+"/"+xml_file_name+"'");
  shell.exec("cp ./csv_pick_all/run_info.csv '"+dir_csv_post+"/"+csv_file_name+"'");
}

app.get('/report_info_list', (req, res)=>{
  let list = [];
  list = (shell.exec("ls -t "+dir_sra+"/reports_info").stdout).split('\n');
  list.pop();
  let list_log = [];
  list_log = (shell.exec("ls -t "+dir_sra+"/reports_log").stdout).split('\n');
  list_log.pop();
  res.render(__dirname+"/views/report_info_list.ejs", {files:list, files_log:list_log});
})

app.get('/look_at_report_info', (req, res)=>{
  if(shell.test('-e', dir_sra+"/reports_info/"+req.query.name)){
    res.sendFile(dir_sra+"/reports_info/"+req.query.name);
  }else{
    res.send('File not found');
  }
    
});

app.post('/remove_report_info', (req, res)=>{
  if(shell.test('-e', dir_sra+"/reports_info/"+req.body.name)){
    shell.exec("rm '"+dir_sra+"/reports_info/"+req.body.name+"'");
    res.redirect("/report_info_list");
  }else{
    res.send('File not found');
  }
    
});

app.get('/look_at_report_log', (req, res)=>{
  if(shell.test('-e', dir_sra+"/reports_log/"+req.query.name)){
    res.sendFile(dir_sra+"/reports_log/"+req.query.name);
  }else{
    res.send('File not found');
  }
    
});

app.post('/remove_report_log', (req, res)=>{
  if(shell.test('-e', dir_sra+"/reports_log/"+req.body.name)){
    shell.exec("rm '"+dir_sra+"/reports_log/"+req.body.name+"'");
    res.redirect("/report_info_list");
  }else{
    res.send('File not found');
  }
    
});

app.get('/report_to_see_dir_list', (req, res)=>{
  let list = [];
  list = (shell.exec("ls "+dir_sra+"/reports_to_see/").stdout).split('\n');
  list.pop();
  res.render(__dirname+"/views/report_to_see_list.ejs", {files:list, dirs:true});
})

app.get('/report_to_see_file_list', (req, res)=>{
  let list = [];
  list = (shell.exec("ls "+dir_sra+"/reports_to_see/"+req.query.name).stdout).split('\n');
  list.pop();
  res.render(__dirname+"/views/report_to_see_list.ejs", {files:list, dirs:false, dir_name:req.query.name});
})

app.get('/look_at_report_to_see', (req, res)=>{
  if(shell.test('-e', dir_sra+"/reports_to_see/"+req.query.dir_name+"/"+req.query.name)){
    res.sendFile(dir_sra+"/reports_to_see/"+req.query.dir_name+"/"+req.query.name);
  }else{
    res.send('File "'+dir_sra+"/reports_to_see/"+req.query.dir_name+"/"+req.query.name+'" not found');
  }
    
});






//########################## DB GUI #####################################################################

app.get("/db_gui", (req, res)=>{
  res.render(__dirname+"/views/db_gui.ejs", {taxon_list_present:false, report_list_present:false});
});

app.get("/get_taxon_by_name", (req, res)=>{
  let search_name="'%"+req.query.taxon_name+"%'";
  if(search_name=="'%undefined%'"){
    res.redirect("/db_gui");
  }else{
    search_name="'%"+req.query.taxon_name.toLowerCase()+"%'";
    const client = new Client();
    client.connect()
      .then(() => {
        let query="select * from taxon where lower(taxonname) like "+search_name+";";
        client.query(query,(error, result) => {
          console.log("Query " + query + " esguita");
          if(error){
            console.log("Errore query "+query);
            client.end();
            res.send(error);
          }
          //res.send(result.rows);
          client.end();
          res.render(__dirname+"/views/db_gui.ejs", {
            taxon_list_present:true, 
            report_list_present:false, 
            taxon_list:result.rows,
            taxon_name:req.query.taxon_name
          });
        })
        
      })
      .catch(() => {
        client.end();
        res.send("Error on DB connection\n");
      });
  }
});

function split_report_by_study_id(report_list){
  let study = [];
  report_list.forEach(row =>{
    study.push(row.studyid);
  })
  study = study.filter((value, index, array) =>{
    return array.indexOf(value) === index;
  });
  let study_obj = {
    "study_ids":study
  }
  study.forEach(study_id =>{
    study_obj[study_id] = [];
  })
  report_list.forEach(row =>{
    study_obj[row.studyid].push(row);
  })
  return study_obj;
}

app.get("/get_report_by_taxon_id", (req, res)=>{
  let taxon_id="'"+req.query.taxon_id+"'";
  if(taxon_id=="'undefined'"){
    res.redirect("/db_gui");
  }else{
    const client = new Client();
    client.connect()
      .then(() => {
        let query="select * from get_reports_from_taxon_id_report("+taxon_id+");";
        let query_ord="select runid,studytitle,taxnamesample,sampleid,rootednum,rootedperc,directnum,directperc,studyid "+
          "from get_reports_from_taxon_id_report("+taxon_id+") "+
          "join report on runaccession = runid "+
          "order by reportdate DESC;"
        client.query(query_ord,(error, result) => {
          console.log("Query " + query_ord + " esguita");
          if(error){
            console.log("Errore query "+query_ord);
            client.end();
            res.send(error);
          }else{
          
            //studyid | studytitle | taxnamesample | runid | publicated | rootednum | rootedperc | directnum | directperc
            let report_list=result.rows;
            let report_object = split_report_by_study_id(report_list);
            console.log(report_object.study_ids);
            let query_tax_name="select taxonname from taxon where taxonid = "+taxon_id+";";
            client.query(query_tax_name,(error, result) => {
              if(error){
                client.end();
                res.render(__dirname+"/views/db_gui.ejs", {
                  taxon_list_present:false, 
                  report_list_present:true, 
                  report_list:report_list,
                  taxon_id:req.query.taxon_id,
                  taxon_name:""
                });
              }else{
                client.end();
                res.render(__dirname+"/views/db_gui_group.ejs", {
                  taxon_list_present:false, 
                  report_list_present:true, 
                  report_obj:report_object,
                  taxon_id:req.query.taxon_id,
                  taxon_name:" ("+result.rows[0].taxonname+")"
                });
                  }
            } );
          }
        })
        
      })
      .catch(() => {
        client.end();
        res.send("Error on DB connection\n");
      });
  }
});

app.get("/get_report_list_by_taxon_id", (req, res)=>{
  let taxon_id="'"+req.query.taxon_id+"'";
  if(taxon_id=="'undefined'"){
    res.redirect("/db_gui");
  }else{
    const client = new Client();
    client.connect()
      .then(() => {
        let query="select * from get_reports_from_taxon_id_report("+taxon_id+");";
        let query_ord="select runid,studytitle,taxnamesample,sampleid,rootednum,rootedperc,directnum,directperc,studyid "+
          "from get_reports_from_taxon_id_report("+taxon_id+") "+
          "join report on runaccession = runid "+
          "order by reportdate DESC;"
        client.query(query_ord,(error, result) => {
          console.log("Query " + query_ord + " esguita");
          if(error){
            console.log("Errore query "+query_ord);
            client.end();
            res.send(error);
          }
          
          //studyid | studytitle | taxnamesample | runid | publicated | rootednum | rootedperc | directnum | directperc
          let report_list=result.rows;
          console.log(report_object.study_ids);
          let query_tax_name="select taxonname from taxon where taxonid = "+taxon_id+";";
          client.query(query_tax_name,(error, result) => {
            if(error){
              client.end();
              res.render(__dirname+"/views/db_gui.ejs", {
                taxon_list_present:false, 
                report_list_present:true, 
                report_list:report_list,
                taxon_id:req.query.taxon_id,
                taxon_name:""
              });
            }else{
              client.end();
              res.render(__dirname+"/views/db_gui.ejs", {
                taxon_list_present:false, 
                report_list_present:true, 
                report_list:report_list,
                taxon_id:req.query.taxon_id,
                taxon_name:" ("+result.rows[0].taxonname+")"
              });
                }
          } );
          
        })
        
      })
      .catch(() => {
        client.end();
        res.send("Error on DB connection\n");
      });
  }
});

function order_descendent(key, in_list, out_list, span=1){
  in_list.forEach(taxon =>{
    if(taxon.parenttaxonid == key & taxon.taxonid!=key){
      let tax_s=taxon;
      for(let i=0; i<span; i++){
        tax_s.taxname="___"+tax_s.taxname;
      }
      out_list.push(tax_s);
      order_descendent(taxon.taxonid, in_list, out_list, span+1);
    }
  })
}

app.get("/get_report_data_by_runaccession", (req, res)=>{
  let run_acc="'"+req.query.run_acc+"'";
  if(run_acc=="'undefined'"){
    res.redirect("/db_gui");
  }else{
    const client = new Client();
    client.connect()
      .then(() => {
        let query_ord="select runaccession, taxon.taxonid, taxonname, rootedfragmentnum,directfragmentnum "+
          "from reporttaxon join taxon on reporttaxon.taxonid = taxon.taxonid "+
          "where runaccession = "+run_acc+" order by parenttaxonid, taxonid;";
        let query_func="select * from get_taxon_from_run_acc("+run_acc+") order by rootednum DESC";
        client.query(query_func,(error, result) => {
          console.log("Query " + query_func + " esguita");
          if(error){
            console.log("Errore query "+query_func);
            client.end();
            res.send(error);
          }

          client.end();
          if(true){
            let ord_list=[];
            ord_list.push(result.rows[0]);
            order_descendent(1, result.rows, ord_list);

            res.render(__dirname+"/views/db_gui_report_data.ejs", {
              report_data_present:true, 
              report_data:ord_list,
              run_acc:req.query.run_acc
            });
          }else{
            res.render(__dirname+"/views/db_gui_report_data.ejs", {
              report_data_present:true, 
              report_data:result.rows,
              run_acc:req.query.run_acc
            });
          }
        })
        
      })
      .catch(() => {
        client.end();
        res.send("Error on DB connection\n");
      });
  }
});

app.get("/get_extra_data_for_sample", (req, res)=>{
  let sample_id="'"+req.query.sample_id+"'";
  if(sample_id=="'undefined'"){
    res.redirect("/db_gui");
  }else{
    const client = new Client();
    client.connect()
      .then(() => {
        let query_func="select tag, value from sampleextra where sampleaccession = "+sample_id+";";
        client.query(query_func,(error, result) => {
          console.log("Query " + query_func + " esguita");
          if(error){
            console.log("Errore query "+query_func);
            client.end();
            res.send(error);
          }else{

            let query_db = "select S.studyaccession,S.title AS study_Title,S.abstract,S.bioprojectid,"+
              "E.experimentaccession,E.title AS experiment_Title,E.alias AS exepriment_Alias,E.design,E.libraryname,E.librarysource,E.librarylayout,E.libraryselection,E.librarystrategy,E.platformname,E.instrumentname,"+
              "SA.title AS sample_Title,SA.alias AS sample_Alias,T.taxonname "+
              "from study AS S join studysample as SS on S.studyaccession = SS.studyaccession and SS.sampleaccession = "+sample_id+" "+
              "join experiment as E on E.sampleaccession = SS.sampleaccession "+
              "join Sample as SA on SA.sampleaccession = E.sampleaccession "+
              "join Taxon as T on SA.taxonid = T.taxonid ;";
            client.query(query_db,(error, result_data) => {
              console.log("Query " + query_db + " esguita");
              if(error){
                console.log("Errore query "+query_db);
                client.end();
                res.send(error);
              }else{
                client.end();
                //console.log(Object.values(result_data.rows[0]));
                res.render(__dirname+"/views/sample_extra.ejs", {
                  sample_id:req.query.sample_id, 
                  data_list:result.rows,
                  db_all_data_list:result_data.rows
                });
              }
            });
            
          }
        })
        
      })
      .catch(() => {
        client.end();
        res.send("Error on DB connection\n");
      });
  }
});










//########################## Home page HTML #####################################################################

app.get('', (req, res)=>{
  res.render(__dirname+"/views/home.ejs");
});

app.get('/style.css', (req, res)=>{
  res.sendFile(__dirname+"/views/style.css");
});

app.post('/upload', 
  fileUpload({createParentPath: true}),
  (req, res)=>{
    const files = req.files;
    console.log(files);
    let prefix = req.body.analysis_name;

    if(prefix=="undefined"){
      res.send("Error: no analysis name found");
    }else{
      let index = prefix.indexOf(" ");
      while(index>=0){
        prefix = prefix.replaceAt(index, "_");
        index = prefix.indexOf(" ");
      }
      index = prefix.indexOf("/");
      while(index>=0){
        prefix = prefix.replaceAt(index, "");
        index = prefix.indexOf("/");
      }
    }
    console.log("Prefix: "+prefix);
    Object.keys(files).forEach(key =>{
      let filepath = path.join(dir_csv_post, files[key].name);
      if(files[key].name.indexOf(".csv")>0){
        filepath = path.join(dir_csv_post, prefix+"_run_info.csv");
      }
      if(files[key].name.indexOf(".xml")>0){
        filepath = path.join(dir_xml_post, prefix+"_fetch.xml");
      }
      
      files[key].mv(filepath, (error)=>{
        if (error){ 
          return res.send(error);
        }else{
          if(files[key].name.indexOf(".xml")>0){
            convert_and_insert_metadata(prefix);
          }
          if(files[key].name.indexOf(".csv")>0){
            shell.cd()
          }
        }
      })
    });
    
    res.send("Upload successfully done, go to the <a href='/csv_files'>CSV file page</a>");
  }

);

function check_xml_file(file_path){
  if(shell.test("-e", file_path)){
    let out = shell.exec("head -n 2 "+file_path+" | grep '<EXPERIMENT_PACKAGE_SET>' | wc -l").stdout;
    return (out.charAt(0)=="1");
  }else{
    console.log("File "+file_path+" does not exist");
    return false
  }
}

app.post('/upload_only_xml', 
  fileUpload({createParentPath: true}),
  (req, res)=>{
    const files = req.files;
    console.log(files);
    let prefix = req.body.analysis_name;

    if(prefix=="undefined"){
      res.send("Error: no analysis name found");
    }else{
      let index = prefix.indexOf(" ");
      while(index>=0){
        prefix = prefix.replaceAt(index, "_");
        index = prefix.indexOf(" ");
      }
      index = prefix.indexOf("/");
      while(index>=0){
        prefix = prefix.replaceAt(index, "");
        index = prefix.indexOf("/");
      }
    }
    console.log("Prefix: "+prefix);
    Object.keys(files).forEach(key =>{
      filepath = path.join(dir_xml_post, prefix+"_fetch.xml");
      
      files[key].mv(filepath, (error)=>{
        if (error){ 
          res.send(error);
        }else{
          if(check_xml_file(filepath)){
            create_csv_from_uploaded_xml(prefix);
            res.send("Upload successfully done, go to the <a href='/csv_files'>CSV file page</a>");
            convert_and_insert_metadata(prefix);
          }else{
            res.send("Error! File does not contain data from SRA but from other databases");
          }
        }
      })
    })
    
    
  }

);


app.listen(port, () => {

  let list = get_activity_list();
  list.forEach(element=>{
    if(element[1].indexOf("God_")==0){
      if(element[2]=="working"){
        update_activity(element[1], "todo", "{Error: Process was stopped by server shutdown}");
        shell.exec("rm -r "+dir_sra_reports+"/*");
      } 
    }
  })

  shell.exec("apt-get install bc");
  shell.exec('curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | tee /etc/apt/sources.list.d/ngrok.list &&  apt update && apt install ngrok');
  shell.exec('ngrok config add-authtoken 2S6onf7fX4v8bdYeWlxODuGNUQ5_3KabNUmt3ooR2uD3PybdE');
  
  shell.exec("rm "+dir_xml_post+"/*");

  shell.cd(); 
  console.log(`App listening on container port ${port}, 49160 dell'host`);
});