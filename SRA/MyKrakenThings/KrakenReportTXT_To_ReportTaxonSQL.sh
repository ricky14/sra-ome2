#set -x


#TABLE ReportTaxon
#(RunAccession, Collection, CollectionDate, TaxonID, RootedFragmentNum, DirectFragmentNum)

folder=$1

all_reports_file='tmp_reports.txt'

collection='Standard-16'
collectionDate='2023-06-05'

pwd
if [ -e '/root/databases/db_coll.txt' ]
then
    collection=`head -n1 /root/databases/db_coll.txt | sed 's:\n.*::'`
    echo $collection
fi
pwd
if [ -e '/root/databases/db_coll_date.txt' ]
then
    collectionDate=`head -n1 /root/databases/db_coll_date.txt | sed 's:\n.*::'`
    echo $collectionDate
fi

output_file='7_rep_ReportTaxon.sql'

find $folder | grep '[D,E,S]RR[0-9]*.kraken.report.txt' >$all_reports_file

i=0
n=`cat $all_reports_file | wc -l`

echo -n >$output_file

while read report
do
    
	progress=`bc <<< "scale=4; ($i/$n)*100"`
	echo $progress'%:' $report

    #report_file_name format: /path/to/RunAccession.kraken.report.txt
    runAccession=`echo "$report" | sed s:.*'/':: | cut -d'.' -f1`

    report_data_file='tmp_report_data.txt'

    if [ -s $report ] 
    then 
        #file exists and is not empty
            
        cat $report | grep -v 'U.*0.*unclassified' | tr '\t' ',' | awk -F',' '{ print $2, $3, $5, $4 }' \
        | sed "s/ /,/" | sed "s/ /,/" | sed "s/ /,'/" > $report_data_file

        prefix=\(\'$collection\'\,\'$collectionDate\'\,\'$runAccession\'\,

        echo -e 'start transaction;\n' >>$output_file
        echo 'insert into ReportTaxon' >>$output_file
        echo '(Collection, CollectionDate, RunAccession, RootedFragmentNum, DirectFragmentNum, TaxonID, Note)' >>$output_file
        echo 'values' >>$output_file

        cat $report_data_file | sed s/^/"$prefix"/ | sed "s/$/'),/" | sed '$ s/,$/ON CONFLICT DO NOTHING;/' >>$output_file

        echo -e '\ncommit;\n' >>$output_file
        echo >>$output_file
        
        rm $report_data_file
        
    else 
        #file not exists or is empty
        echo '----------- EMPTY FILE IGNORED:' $report
    fi

    i=$(($i + 1))

done < $all_reports_file

rm $all_reports_file


