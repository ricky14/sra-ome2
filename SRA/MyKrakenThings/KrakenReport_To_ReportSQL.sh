#set -x

# TABLE REPORT
# (RunAccession, Collection, CollectionDate, ReportDate, Note)

main_folder=$1
output_file='6_rep_Report.sql'
temp_file='temp'

echo -n >$temp_file

report_dates_file='reports_dates.txt'

ls $main_folder -l \
| grep ^d \
| egrep '[0-9]{4,4}_[0-9]{2,2}_[0-9]{2,2}' \
| tr -s ' ' \
| cut -d' ' -f9 \
>$report_dates_file

ls $main_folder -l \
| grep ^d \
| tr -s ' ' \
| cut -d' ' -f9 \
>$report_dates_file

collection='Standard-16'
collectionDate='2023-06-05'
pwd
if [ -e '/root/databases/db_coll.txt' ]
then
    collection=`head -n1 /root/databases/db_coll.txt | sed 's:\n.*::'`
    echo $collection
fi
pwd
if [ -e '/root/databases/db_coll_date.txt' ]
then
    collectionDate=`head -n1 /root/databases/db_coll_date.txt | sed 's:\n.*::'`
    echo $collectionDate
fi

output_file_outcome='8_rep_RunOutcome.sql'
echo 'start transaction;'> $output_file_outcome

while read report_name
do

    all_runs_file='runs.txt'
    ls $main_folder/$report_name -l | grep '[D,E,S]RR[0-9]*' | tr -s ' '| cut -d' ' -f9 >$all_runs_file

    report_date=`echo $report_name | egrep -o '[0-9]{4,4}_[0-9]{2,2}_[0-9]{2,2}'`

    while read run
    do
        line="('$collection','$collectionDate','$report_date','$run')," 
        echo $line >>$temp_file

    done < "$all_runs_file"

    while read line
    do
        run_id=`echo -n $line | sed "s/,.*//"`
        run_outcome=`echo -n $line | sed "s/^.*,//"`
        echo "UPDATE run SET runoutcome = '$run_outcome' WHERE runaccession='$run_id' AND runoutcome != 'OK';" >> $output_file_outcome
    done < "$main_folder/$report_name/.info/results_all.csv"


done < "$report_dates_file"

echo 'commit;' >>$output_file_outcome



echo 'start transaction;' >$output_file
echo >>$output_file
echo 'insert into Report' >>$output_file
echo '(Collection, CollectionDate, ReportDate, RunAccession)' >>$output_file
echo 'values' >>$output_file
sed '$ s/,$/ON CONFLICT DO NOTHING;/' $temp_file >>$output_file
echo >>$output_file
echo 'commit;' >>$output_file

rm $temp_file
rm $report_dates_file
rm $all_runs_file




