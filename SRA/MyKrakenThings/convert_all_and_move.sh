#!/bin/bash

report_dir="../reports_to_insert"
csv_name=`echo -n "$1" | sed "s/\.csv//"`

./KrakenReport_To_ReportSQL.sh $report_dir
status=$?
if [ $status -eq 0 ]
then
    ./KrakenReportTXT_To_ReportTaxonSQL.sh $report_dir
    status=$?
    if [ $status -eq 0 ]
    then
        cp ./*.sql ../sql/7_kraken_reports
        now=$(date +'%Y-%m-%d-%T')
        cp -f ../reports_to_insert/*/.info/results_all.csv "../reports_info/$csv_name-$now-info.csv"

        errors=`cat ../reports_to_insert/*/.info/results_all.csv | grep ",ERR" | wc -l`
        if [ $errors -gt 0 ]
        then 
            echo "Error logs:" > "../reports_log/$csv_name-$now-log-ERROR.txt"
            cat ../reports_to_insert/*/.info/log.txt | grep -B1 -A1 "error" >> "../reports_log/$csv_name-$now-log-ERROR.txt"
        fi

        warns=`cat ../reports_to_insert/*/.info/log.txt | egrep "warning.* layout is not" | wc -l`
        if [ $warns -gt 0 ]
        then 
            echo "Warning logs:" > "../reports_log/$csv_name-$now-log-WARNING.txt"
            cat ../reports_to_insert/*/.info/log.txt | egrep -A2 "warning.* layout is not" >> "../reports_log/$csv_name-$now-log-WARNING.txt"
        fi
        
        #if [ ! -e "../reports_to_see/$csv_name-$now" ]
        #then
        #    mkdir ../reports_to_see/$csv_name-$now
        #fi
        #mv ../reports_to_insert/*/*/*.txt ../reports_to_see/$csv_name-$now/
        rm -r ../reports_to_insert/*
    else
        exit $status
    fi
else
    exit $status
fi



