#!/bin/bash

i=0
c=0
file_path=$1
file_name=`echo -n $1 | sed "s/\.csv//"`

if [ $# -eq 2 ]
then
    n=$2
else
    n=4
fi

while read line
do
    if [ $i -eq 0 ]
    then
        if [ $c -eq 0 ]
        then
            first_line="$line"
            c=1
            i=$((-1))
        else
            echo $first_line > $file_name"_god_split_$c.csv"
            echo $line >> $file_name"_god_split_$c.csv"
        fi
    else
        echo $line >> $file_name"_god_split_$c.csv"
    fi

    i=$(( $i + 1 ));
    if [ $i -eq $n ]
    then
        i=0
        c=$(( $c + 1 ))
    fi

done < $file_path

#rm $file_path

