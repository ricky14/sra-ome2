start transaction;

insert into Lineage
(Rank, ParentRank)
values
('biotype','species'),
('clade','subfamily'),
('clade','genus'),
('clade','order'),
('clade','tribe'),
('clade','parvorder'),
('clade','infraorder'),
('clade','superclass'),
('clade','subcohort'),
('clade','infraclass'),
('clade','subphylum'),
('clade','no rank'),
('clade','family'),
('clade','superorder'),
('clade','superfamily'),
('clade','superkingdom'),
('clade','suborder'),
('clade','species'),
('clade','subclass'),
('clade','clade'),
('clade','section'),
('clade','subtribe'),
('clade','kingdom'),
('clade','cohort'),
('clade','subgenus'),
('clade','varietas'),
('clade','phylum'),
('clade','class'),
('class','no rank'),
('class','clade'),
('class','subphylum'),
('class','superclass'),
('class','phylum'),
('class','superkingdom'),
('cohort','infraclass'),
('cohort','no rank'),
('family','superfamily'),
('family','subphylum'),
('family','superkingdom'),
('family','subclass'),
('family','infraorder'),
('family','suborder'),
('family','parvorder'),
('family','superorder'),
('family','kingdom'),
('family','clade'),
('family','phylum'),
('family','order'),
('family','no rank'),
('family','infraclass'),
('family','class'),
('forma','species'),
('forma','subspecies'),
('forma','varietas'),
('forma specialis','varietas'),
('forma specialis','species'),
('forma specialis','forma specialis'),
('genotype','species'),
('genus','superfamily'),
('genus','tribe'),
('genus','subclass'),
('genus','phylum'),
('genus','class'),
('genus','superkingdom'),
('genus','order'),
('genus','suborder'),
('genus','subtribe'),
('genus','subfamily'),
('genus','family'),
('genus','no rank'),
('genus','kingdom'),
('genus','clade'),
('genus','infraclass'),
('infraclass','phylum'),
('infraclass','clade'),
('infraclass','subclass'),
('infraclass','class'),
('infraorder','suborder'),
('infraorder','clade'),
('infraorder','order'),
('isolate','strain'),
('isolate','forma specialis'),
('isolate','species'),
('isolate','clade'),
('isolate','subspecies'),
('kingdom','superkingdom'),
('kingdom','clade'),
('morph','species'),
('no rank','kingdom'),
('no rank','section'),
('no rank','subclass'),
('no rank','parvorder'),
('no rank','species group'),
('no rank','superclass'),
('no rank','subfamily'),
('no rank','superorder'),
('no rank','subkingdom'),
('no rank','biotype'),
('no rank','genus'),
('no rank','clade'),
('no rank','subspecies'),
('no rank','cohort'),
('no rank','class'),
('no rank','serogroup'),
('no rank','suborder'),
('no rank','infraorder'),
('no rank','strain'),
('no rank','genotype'),
('no rank','pathogroup'),
('no rank','superfamily'),
('no rank','tribe'),
('no rank','no rank'),
('no rank','species subgroup'),
('no rank','phylum'),
('no rank','serotype'),
('no rank','varietas'),
('no rank','species'),
('no rank','subphylum'),
('no rank','superkingdom'),
('no rank','subgenus'),
('no rank','order'),
('no rank','subtribe'),
('no rank','infraclass'),
('no rank','family'),
('order','no rank'),
('order','class'),
('order','subphylum'),
('order','superkingdom'),
('order','clade'),
('order','phylum'),
('order','superorder'),
('order','subclass'),
('order','cohort'),
('order','infraclass'),
('order','subcohort'),
('parvorder','infraorder'),
('parvorder','order'),
('parvorder','suborder'),
('pathogroup','species'),
('phylum','clade'),
('phylum','superphylum'),
('phylum','superkingdom'),
('phylum','subkingdom'),
('phylum','no rank'),
('phylum','kingdom'),
('section','clade'),
('section','subgenus'),
('section','no rank'),
('section','genus'),
('series','section'),
('series','subsection'),
('serogroup','species'),
('serotype','species'),
('serotype','no rank'),
('serotype','serogroup'),
('species','subfamily'),
('species','clade'),
('species','suborder'),
('species','order'),
('species','species subgroup'),
('species','genus'),
('species','tribe'),
('species','phylum'),
('species','family'),
('species','subsection'),
('species','subtribe'),
('species','species group'),
('species','class'),
('species','subgenus'),
('species','no rank'),
('species','series'),
('species','section'),
('species','superfamily'),
('species','subclass'),
('species group','subfamily'),
('species group','series'),
('species group','family'),
('species group','no rank'),
('species group','genus'),
('species group','section'),
('species group','clade'),
('species group','subgenus'),
('species subgroup','species group'),
('strain','forma'),
('strain','biotype'),
('strain','clade'),
('strain','serotype'),
('strain','serogroup'),
('strain','subspecies'),
('strain','forma specialis'),
('strain','no rank'),
('strain','varietas'),
('strain','species'),
('subclass','phylum'),
('subclass','class'),
('subclass','clade'),
('subclass','no rank'),
('subcohort','cohort'),
('subfamily','suborder'),
('subfamily','family'),
('subfamily','clade'),
('subfamily','no rank'),
('subfamily','order'),
('subfamily','superfamily'),
('subgenus','genus'),
('subgenus','no rank'),
('subgenus','clade'),
('subkingdom','kingdom'),
('suborder','order'),
('suborder','phylum'),
('suborder','class'),
('suborder','no rank'),
('suborder','clade'),
('subphylum','clade'),
('subphylum','phylum'),
('subsection','section'),
('subspecies','clade'),
('subspecies','species'),
('subspecies','genus'),
('subspecies','no rank'),
('subtribe','family'),
('subtribe','no rank'),
('subtribe','subfamily'),
('subtribe','tribe'),
('subtribe','clade'),
('subvariety','species'),
('superclass','subphylum'),
('superclass','clade'),
('superfamily','subclass'),
('superfamily','suborder'),
('superfamily','parvorder'),
('superfamily','no rank'),
('superfamily','superorder'),
('superfamily','class'),
('superfamily','infraorder'),
('superfamily','order'),
('superfamily','phylum'),
('superfamily','clade'),
('superkingdom','no rank'),
('superorder','class'),
('superorder','infraclass'),
('superorder','clade'),
('superorder','cohort'),
('superorder','subclass'),
('superphylum','clade'),
('tribe','subfamily'),
('tribe','no rank'),
('tribe','family'),
('tribe','clade'),
('varietas','species'),
('varietas','subspecies') ON CONFLICT DO NOTHING;

commit;