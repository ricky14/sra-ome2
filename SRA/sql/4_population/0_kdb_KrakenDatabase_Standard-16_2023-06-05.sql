start transaction;

insert into KrakenDatabase
(Collection, CollectionDate, ArchiveSizeGB, IndexSizeGB, CappedAtGB)
values
('Standard-8','2023-06-05','5.5','7.5','16') ON CONFLICT DO NOTHING;

commit;
