#!/bin/bash

kraken_file=$(ls ./4_population/3_kdb*)
taxon_file=$(ls ./4_population/2_tax*)
temp_file="temp_tax.txt"
out_file="missing_taxon.txt"

kraken_tax_id=`cat "$kraken_file" \
| grep "('" \
| sed "s/).*//g" \
| grep -Eo '[0-9]+$' \
| sort | uniq | sort `


cat "$taxon_file" \
| grep "('" \
| sed "s/('//g" \
| grep -Eo '^[0-9]+' \
| sort | uniq | sort \
> $temp_file

[ -e $out_file ] && rm $out_file

for id_t in $kraken_tax_id
do
    result=`cat $temp_file \
    | egrep "^$id_t$"`

    echo -ne "Id: "$id_t'\r'
    if [ "$result" == "" ]
    then
        echo $id_t >> $out_file
    #else
        #echo "Result: '$result'"
    fi

done
echo ""
wc -l $out_file
rm $temp_file
