This Taxon is an example of the possibility for taxons to change taxonid
Searching 220390 in taxonomy will redirect you to 2830794
More over:
     - in metadata file of 03/2021 there are same samples with sample taxonid = 220390
     - in metadata file of 07/2021 the same samples have sample taxonid = 2830794

Old Taxonomy ID: 220390
New Taxonomy ID: 2830794
