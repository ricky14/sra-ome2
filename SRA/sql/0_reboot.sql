-- destroy database
-- use it ONLY in testing phase!!!
-- all data will be lost!!!!
\c postgres;
SELECT pg_terminate_backend( pid )
FROM pg_stat_activity
WHERE pid <> pg_backend_pid( )
    AND datname = 'sra_analysis';
drop database sra_analysis;
create database sra_analysis;
\c sra_analysis;
