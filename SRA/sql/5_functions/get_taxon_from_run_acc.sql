
create or replace function get_taxon_from_run_acc(
    _run_acc dom_run_accession
)
returns table(
    RunID               dom_run_accession,
    TaxonID       dom_taxon_id,
    ParentTaxonID       dom_taxon_id,
    TaxName       text,
    RootedNum      bigint,
    RootedPerc     decimal,
    DirectNum      bigint,
    DirectPerc     decimal,
    ReportRank     text
)
language plpgsql as
$$
begin
    return query
    select
        RU.RunAccession                                         as RunID,
        TaxOfReport.TaxonID                                     as TaxonID,
        TaxOfReport.ParentTaxonID                               as ParentTaxonID,
        left(TaxOfReport.TaxonName,20)                          as TaxName,
        RT.RootedFragmentNum                                    as RootedNum,
        get_percentage(RT.RootedFragmentNum, RU.Spot)           as RootedPerc,
        RT.DirectFragmentNum                                    as DirectNum,
        get_percentage(RT.DirectFragmentNum, RU.Spot)           as DirectPerc,
        RT.note                                                 as ReportRank

    from 
        Taxon as TaxOfReport

    join ReportTaxon as RT              on RT.TaxonID               = TaxOfReport.TaxonID
    join Report      as RE              on RE.RunAccession          = RT.RunAccession
    join Run         as RU              on RU.RunAccession          = RE.RunAccession
    join Experiment  as E               on E.ExperimentAccession    = RU.ExperimentAccession
    join Sample      as SA              on SA.SampleAccession       = E.SampleAccession
    join StudySample as SS              on SS.SampleAccession       = SA.SampleAccession
    join Study       as ST              on ST.StudyAccession        = SS.StudyAccession
    join Taxon       as TaxNameSample   on SA.TaxonID               = TaxNameSample.TaxonID

    where
        RE.RunAccession = _run_acc

    order by 
        TaxOfReport.taxonid  asc
    ;

end;
$$;