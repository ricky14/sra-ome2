#!/bin/bash

cd /root/SRA/sql
sql_runner='./run_sql.sh'

user='rrossetto'
db='sra_analysis'
main_folder='.'

pg_log_file='log_postgres.log'
commands_errors_log_file='log_commands_and_errors.log'

echo -n >$pg_log_file
echo -n >$commands_errors_log_file

echo $(date) >>$commands_errors_log_file

#1)
echo "creating db..." | tee -a $commands_errors_log_file
create_db_file=$main_folder'/1_create.sql'
$sql_runner $create_db_file $user $db 1> >(tee -a $pg_log_file >&1) 2> >(tee -a $commands_errors_log_file >&2)

#2)
echo "adding constraints..." | tee -a $commands_errors_log_file
constraints_file=$main_folder'/2_constraints.sql'
$sql_runner $constraints_file $user $db 1> >(tee -a $pg_log_file >&1) 2> >(tee -a $commands_errors_log_file >&2)

#3)
echo "adding triggers..." | tee -a $commands_errors_log_file
triggers_file=$main_folder'/3_triggers.sql'
$sql_runner $triggers_file $user $db 1> >(tee -a $pg_log_file >&1) 2> >(tee -a $commands_errors_log_file >&2)