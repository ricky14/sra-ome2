#!/bin/bash

cd /root/SRA/sql
sql_runner='./run_sql.sh'

user='rrossetto'
db='sra_analysis'
main_folder='.'

pg_log_file='log_postgres.log'
commands_errors_log_file='log_commands_and_errors.log'

echo -n >$pg_log_file
echo -n >$commands_errors_log_file

echo $(date) >>$commands_errors_log_file

#0)
echo "deleting db..." | tee -a $commands_errors_log_file
delete_db_file=$main_folder'/0_reboot.sql'
$sql_runner $delete_db_file $user $db 1> >(tee -a $pg_log_file >&1) 2> >(tee -a $commands_errors_log_file >&2)

status=$?
if [ $status != 0 ]
then
    exit $status
fi

sleep 2

#1)
echo "creating db..." | tee -a $commands_errors_log_file
create_db_file=$main_folder'/1_create.sql'
$sql_runner $create_db_file $user $db 1> >(tee -a $pg_log_file >&1) 2> >(tee -a $commands_errors_log_file >&2)

status=$?
if [ $status != 0 ]
then
    exit $status
fi

#2)
echo "adding constraints..." | tee -a $commands_errors_log_file
constraints_file=$main_folder'/2_constraints.sql'
$sql_runner $constraints_file $user $db 1> >(tee -a $pg_log_file >&1) 2> >(tee -a $commands_errors_log_file >&2)

status=$?
if [ $status != 0 ]
then
    exit $status
fi

#3)
echo "adding triggers..." | tee -a $commands_errors_log_file
triggers_file=$main_folder'/3_triggers.sql'
$sql_runner $triggers_file $user $db 1> >(tee -a $pg_log_file >&1) 2> >(tee -a $commands_errors_log_file >&2)

status=$?
if [ $status != 0 ]
then
    exit $status
fi


cd /root/SRA/app_post_csv
rm -r *
cd /root/SRA/app_post_xml
rm -r *
cd /root/SRA/reports
rm -r *
cd /root/SRA/reports_info
rm -r *
cd /root/SRA/reports_log
rm -r *
cd /root/SRA/reports_to_insert
rm -r *
cd /root/SRA/reports_to_see
rm -r *


exit 0
