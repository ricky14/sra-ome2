#!/bin/bash

kraken_file=$(ls ./4_population/3_kdb*)

cat $kraken_file > "./4_population/3bis_kdb_KrakenRecord_checked.sql"
kraken_file="./4_population/3bis_kdb_KrakenRecord_checked.sql"

out_file="missing_taxon.txt"
missing_taxon=`cat $out_file`

for id_t in $missing_taxon
do
    #echo -ne "Replacing: "$id_t'\r'
    #cat $kraken_file > temp
    #cat temp \
    #| sed "s/,$id_t)/,0)/g" \
    #> $kraken_file
    #rm temp

    echo -ne "Replacing: "$id_t'\r'
    cat $kraken_file > temp
    cat temp \
    | sed "s/,$id_t)/,null)/g" \
    > $kraken_file
    rm temp
    
done

cat $kraken_file > temp
cat temp \
| sed "s/^.*,null),/exit/g" \
| sed "s:exit.*::" \
> $kraken_file
rm temp

echo ""

