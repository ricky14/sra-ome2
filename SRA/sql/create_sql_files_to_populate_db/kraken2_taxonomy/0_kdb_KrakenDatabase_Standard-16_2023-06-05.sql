start transaction;

insert into KrakenDatabase
(Collection, CollectionDate, ArchiveSizeGB, IndexSizeGB, CappedAtGB)
values
('Standard-16','2023-06-05','11','15','16') ON CONFLICT DO NOTHING;

commit;
