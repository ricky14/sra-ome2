#set -x

nOfParamsNeeded=1
if test $# -ne $nOfParamsNeeded
then
    echo "usage: $0 <collection_date>"
    echo 'example:' $0 '2020-12-02'
    exit 1
fi

collection_date=$1

rm ../../4_population/0_kdb*
rm ../../4_population/3_kdb*
cp *_$collection_date.sql ../../4_population/

status=$?
if [ $status -eq 0 ]
then
    cd ../../
    #./getMissingTaxonForKraken.sh
    #rm ./4_population/3bis_*  #serve nella versione con eliminazione delle righe
    rm ./4_population/2bis_*  
    ./fixMissingTaxonForKraken.sh
    #rm ./4_population/3_kdb_* #serve nella versione con eliminazione delle righe
else
    echo "Problema nello spostamento dei file"
    exit $status
fi

