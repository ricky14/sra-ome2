#set -x

nOfParamsNeeded=4
if test $# -ne $nOfParamsNeeded
then
    echo "usage: $0 <Inspect file URL> <collection_date> <ArchiveSizeGB> <IndexSizeGB>"
    echo 'example:' $0 'https://genome-idx.s3.amazonaws.com/kraken/standard_16gb_20201202/inspect.txt' '2020-12-02' '11' '15'
    exit 1
fi

inspect_url=$1
collection="Standard-16"
collection_date=$2
archive_size=$3
index_size=$4

index_file_name='Kraken2_'$collection'_'$collection_date'_inspect.txt'

wget $inspect_url -O "$index_file_name"

status=$?
if [ $status -eq 0 ]
then
    ./create_KrakenDatabase_table_from_Kraken2_Taxonomy.sh "Standard-16" $collection_date $archive_size $index_size '16'
    ./create_KrakenRecord_table_from_Kraken2_Taxonomy.sh $index_file_name "Standard-16" $collection_date
    status=$?
    if [ $status -ne 0 ]
    then
        rm *.sql
        echo "Problema nella creazione dei file sql dal file inspect.txt scaricato"
        exit $status
    else
        ./copy_and_fix_taxon.sh $collection_date
    fi
    rm *_inspect.txt
else
    echo "Problema nel download del file inspect.txt"
    exit $status
fi

