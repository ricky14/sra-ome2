#set -x

nOfParamsNeeded=5
if test $# -ne $nOfParamsNeeded
then
    echo "usage: $0 <collection_name> <collection_date> <ArchiveSizeGB> <IndexSizeGB> <CappedAtGB<"
    echo 'example:' $0 'Standard-16' '2020-12-02' '11' '15' '16'
    echo 'output_file: 0_kdb_KrakenDatabase_<collection_name>_<collection_date>.sql'
    exit 1
fi

collection=$1
collection_date=$2
archive_size=$3
index_size=$4
capped_at_size=$5

output_file='0_kdb_KrakenDatabase_'$collection'_'$collection_date'.sql'


echo 'start transaction;' >$output_file
echo >>$output_file
echo 'insert into KrakenDatabase' >>$output_file
echo '(Collection, CollectionDate, ArchiveSizeGB, IndexSizeGB, CappedAtGB)' >>$output_file
echo 'values' >>$output_file
echo "('$collection','$collection_date','$archive_size','$index_size','$capped_at_size') ON CONFLICT DO NOTHING;" >>$output_file
echo >>$output_file
echo 'commit;' >>$output_file

