

wget "$1"
if [ -e "$2" ]
then
    unzip *.zip -u -d "$2"
else
    mkdir "$2"
    unzip *.zip -d "$2"
fi
./create_Taxon_table_from_NCBI_Taxonomy.sh $2 $3
rm *.zip
rm *.sql
mv "./$2/2_tax_Taxon_$3.sql" "./2_tax_Taxon_$3.sql"
rm ../../4_population/2_tax_Taxon*
cp "./2_tax_Taxon_$3.sql" ../../4_population/