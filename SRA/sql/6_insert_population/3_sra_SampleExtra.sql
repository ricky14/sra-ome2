start transaction;

insert into SampleExtra
(SampleAccession,Tag,Value)
values
(E'SRS4109622',E'isolation_source',E'soil'),
(E'SRS4109622',E'collection_date',E'2014-05'),
(E'SRS4109622',E'geo_loc_name',E'Italy: Piedmont'),
(E'SRS4109622',E'lat_lon',E'44.6837 N 8.6258889 E'),
(E'SRS4109622',E'BioSampleModel',E'Metagenome or environmental'),
(E'SRS4109621',E'isolation_source',E'soil'),
(E'SRS4109621',E'collection_date',E'2014-07'),
(E'SRS4109621',E'geo_loc_name',E'Italy: Piedmont'),
(E'SRS4109621',E'lat_lon',E'44.6837 N 8.6258889 E'),
(E'SRS4109621',E'BioSampleModel',E'Metagenome or environmental'),
(E'SRS4109620',E'isolation_source',E'soil'),
(E'SRS4109620',E'collection_date',E'2014-05'),
(E'SRS4109620',E'geo_loc_name',E'Italy: Piedmont'),
(E'SRS4109620',E'lat_lon',E'44.6837 N 8.6258 E'),
(E'SRS4109620',E'BioSampleModel',E'Metagenome or environmental'),
(E'SRS4109619',E'isolation_source',E'soil'),
(E'SRS4109619',E'collection_date',E'2014-07'),
(E'SRS4109619',E'geo_loc_name',E'Italy: Piedmont'),
(E'SRS4109619',E'lat_lon',E'44.6837 N 8.6258 E'),
(E'SRS4109619',E'BioSampleModel',E'Metagenome or environmental') ON CONFLICT DO NOTHING;

commit;
