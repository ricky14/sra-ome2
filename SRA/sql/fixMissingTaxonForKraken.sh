#!/bin/bash

kraken_file=$(ls ./4_population/3_kdb*)

output_file="./4_population/2bis_Taxon_from_kraken.sql"



echo 'start transaction;' >$output_file
echo >>$output_file
echo 'insert into Taxon' >>$output_file
echo '(TaxonID, ParentTaxonID, Rank, TaxonName)' >>$output_file
echo 'values' >>$output_file


while read -r taxid;
do
    if [ $taxid != "" ]
    then
        echo "('$taxid','0','no rank',E'Taxon not present')," >>$output_file
    fi

done < "missing_taxon.txt"

echo "('0','0','no rank',E'Taxon not present') ON CONFLICT DO NOTHING;" >>$output_file
echo >>$output_file
echo 'commit;' >>$output_file

