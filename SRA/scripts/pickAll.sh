#set -x

nOfParamsNeeded=6

if test $# -lt $nOfParamsNeeded
then
    echo "assumption: file char separator ','"
    echo "usage: $0 <inputFile> <groupingFieldIdx> <valueFieldIdx:<run> <layout> <size>> <outputDir> [<MAX_SIZE_MB>]"
    echo "example: $0 metadata_filtered_small_todo.csv 9 8 14 17 /path/to/outputDir"
    exit 1
fi

inputFile=$1
groupingFieldIdx=$2     #scientific name
valueFieldIdx1=$3       #run
valueFieldIdx2=$4       #layout
valueFieldIdx3=$5       #size [MB]
outputDir=`echo "$6" | sed s:/$::`
mkdir $outputDir 2>/dev/null
#doneFieldIdx=$6        #done == NO
MAX_SIZE_MB=10000       #size [MB]

if test $# -gt $nOfParamsNeeded
then
    MAX_SIZE_MB=$7
fi


#output and log files
outputFile=$outputDir/'runs_list.csv'
logFile=$outputDir/'runs_list_log.txt'
echo -n >$outputFile
echo -n >$logFile

delimiter=','

n=0
while read line;
do
    if [ $n -eq 0 ]
    then
        echo "Head: " $(echo $line | cut -d$delimiter -f$valueFieldIdx1,$valueFieldIdx2,$valueFieldIdx3)
        n=1
    else
        #echo $line
        lineCutted=`echo $line | cut -d$delimiter -f$valueFieldIdx1,$valueFieldIdx2,$valueFieldIdx3`
        echo $lineCutted >> $outputFile
    fi
done < $inputFile

exit 0