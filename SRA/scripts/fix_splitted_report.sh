#!/bin/bash

reports_info_dir="$1"

cd "$reports_info_dir"

names=`ls | grep "_god_split_" | sed "s/_god_split_.*//" | uniq | sort | uniq`

for prefix in $names
do
    now=$(date +'%Y-%m-%d-%T')
    files=`ls | grep "$prefix"_god_split_`
    union_file="$prefix""_split_union_"$now".csv"
    echo -n "" > $union_file

    for file_name in $files
    do
        cat $file_name >> $union_file
        rm $file_name
    done 

done 

if [ -e "../reports_log" ]
then
    cd "../reports_log"

    names=`ls | grep "_god_split_" | sed "s/_god_split_.*//" | uniq | sort | uniq`

    for prefix in $names
    do
        now=$(date +'%Y-%m-%d-%T')
        files=`ls | grep "$prefix"_god_split_`
        union_file="$prefix""_split_union_"$now"-log.txt"
        echo -n "" > $union_file

        for file_name in $files
        do
            cat $file_name >> $union_file
            rm $file_name
        done 

    done 

fi

if [ -e "../reports_to_see" ]
then
    cd "../reports_to_see"

    names=`ls | grep "_god_split_" | sed "s/_god_split_.*//" | uniq | sort | uniq`

    for prefix in $names
    do
        now=$(date +'%Y-%m-%d-%T')
        dirs=`ls | grep "$prefix"_god_split_`
        union_dir="$prefix""_split_union_"$now
        mkdir $union_dir

        for dir_name in $dirs
        do
            mv $dir_name"/"* $union_dir
            rm -r $dir_name
        done 

    done 

fi
