
if [ -e activity.txt ]
then
    echo "Deframmentazione"
else
    exit 0
fi


touch activity_def.txt
n=0
for row in $( cat activity.txt | sed "s/^[0-9]*,/,/" )
do
    n=$(( n + 1 ))
    echo $n$row >> activity_def.txt
done

cat activity_def.txt > activity.txt
rm activity_def.txt