
if [ -e activity.txt ]
then
    echo -n "["
else
    echo "[]"
    exit 0
fi

n=0

while read -r row
do
    if [ $n -eq 0 ]
    then
        echo -n "[$row]"
        n=1
    else
        echo -n ",[$row]"
    fi
done < activity.txt
echo "]"