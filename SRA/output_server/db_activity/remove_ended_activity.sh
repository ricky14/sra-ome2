
cat activity.txt \
| grep -v ",closed," \
| grep -v ",done," \
| grep -v ",error," \
> temp.txt

cat temp.txt > activity.txt
rm temp.txt

if [ $# -eq 1 ]
then
    rm activity.txt
fi