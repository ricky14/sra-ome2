
if [ $# -eq 2 ]
then
    data=`grep ",$1," activity.txt | sed 's/^.*{/{/' | sed 's/}.*$/}/' `
else
    data=$3
fi

act_name=`grep ",$1," activity.txt | sed 's/^[0-9]*,//' | sed 's/,.*//'`

if [ $act_name==$1 ]
then
    cat activity.txt \
    | sed "s/,$1,.*$/,$1,$2,$data/" \
    > temp.txt

    cat temp.txt > activity.txt
    rm temp.txt
fi
