
if [ -e "activity.txt" ]
then

    exist=$( cat activity.txt | grep ",$1," | wc -l )
    
    if [ $exist != 0 ]
    then
        echo "Name not valid"
        exit 0
    fi

    last=$( tail -n 1 activity.txt )
    n=`echo $last | sed -r 's/^[^0-9]*([0-9]+).*$/\1/'`
    n=$(( n + 1 ))
else
    touch activity.txt
    n=1
fi

echo $n","$1","$2","$3 >> activity.txt

echo $n